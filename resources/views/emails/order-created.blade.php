@component('mail::message')


<div class="row text-center">
    <center>
    <div class="col-sm-6 col-sm-offset-3">
    <br><br> <h2 style="color:#2E7BFF">Order anda telah berhasil !</h2>

    <h4>Terima kasih, {{$order->alamat->nama_depan.' '.$order->alamat->nama_belakang}} atas pembelian produk di website kami !</h4>
    <h4>No. Order anda {{ $order->order_kode }}</h4>
    <p style="font-size:20px;color:#5C5C5C;">Total biaya pembayaran anda adalah {{format_rupiah($order->total + $order->kurir->harga)}}</p>
    <p style="font-size:20px;color:#5C5C5C;">Segera kirim pembayaran ke Rekening BCA, AN: X No. Rekening : 01234567889</p>
    <br>

<br><br>
    <table border=1>
        <thead>
            <th width="10%">No.</th>
            <th width="50%">Nama Barang</th>
            <th width="10%">Quantity</th>
            <th width="30%">Harga</th>
        </thead>
        <tbody>
            @php ($no = 1)
            {{-- {{  }}dd($order_produks)}} --}}
            @foreach ($order_produks as $order_produk)
            <tr>
                <td>{{ $no }}</td>
                <td>{{ $order_produk->name }}</td>
                <td>{{ $order_produk->quantity }}</td>
                <td>{{ format_rupiah($order_produk->price) }}</td>
            </tr>


            @php($no++)
            @endforeach
            <tr>
                <td colspan="2">{{ $order->kurir->nama }}</td>
                <td><b>Durasi pengiriman 2-3 hari</b></td>
                <td align="right">{{ format_rupiah($order->kurir->harga) }}</td>

            </tr>
            <tr>
                <td colspan="2">Total</td>
                <td colspan="2" align="right">{{ format_rupiah($order->total + $order->kurir->harga) }}</td>
            </tr>
        </tbody>
    </table>
    </div>
    </center>


</div>

@component('mail::button', ['url' => url('/frontend/produk-list')])
Lanjut Belanja
@endcomponent
<br>

<form action =" {{ route('konfirmasi_pembayaran', $order->id) }}" method="POST">
    @csrf
    <input name="_method" type="hidden" value="PATCH">
    <input type="hidden" name="status_bayar" value="confirmed">
    <div class="form-group">
        <button type="submit" name="submit" class="btn btn-primary">Konfirmasi Pembayaran</button>
    </div>
</form>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
