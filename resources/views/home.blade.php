@extends('layouts.utama')

@section('content')

<div class="">
  <div class="">

    <section class="content-header">
      <h1>
       Laravel 5.7
        <small>testing</small>
      </h1>
      {{-- @section('breadcumb')
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">Blank page</li>
      </ol>
      @endsection
    </section> --}}

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Dashboard</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">

          <div class="col-md-3">
            <!-- small box -->
            <div class="small-box bg-orange">
              <div class="inner">
                <h3>{{ $count_kategori }}</h3>
                <p>Kategori</p>
              </div>
              <div class="icon">
                <i class="ion-ios-keypad"></i>
              </div>
            </div>
          </div>

          <div class="col-md-3">
            <!-- small box -->
            <div class="small-box bg-green">
              <div class="inner">
                <h3>{{ $count_produk }}</h3>
                <p>Produk</p>
              </div>
              <div class="icon">
                <i class="fa fa-shopping-bag"></i>
              </div>
            </div>
          </div>

          <div class="col-md-3">
            <!-- small box -->
            <div class="small-box bg-blue">
              <div class="inner">
                <h3>{{ $count_order }}</h3>
                <p>Order</p>
              </div>
              <div class="icon">
                <i class="ion ion-ios-cart"></i>
              </div>
            </div>
          </div>

          <div class="col-md-3">
            <!-- small box -->
            <div class="small-box bg-yellow">
              <div class="inner">
                <h3>{{ $count_customer }}</h3>
                <p>Customer</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
            </div>
          </div>

          <div class="col-md-3">
            <!-- small box -->
            <div class="small-box bg-red">
              <div class="inner">
                <h3>{{ $count_produk_terjual }}</h3>
                <p>Produk Terjual</p>
              </div>
              <div class="icon">
                <i class="ion ion-ios-undo-outline"></i>
              </div>
            </div>
          </div>

          <div class="col-md-3">
            <!-- small box -->
            <div class="small-box bg-purple">
              <div class="inner">
                <h3>{{ $count_order_sukses }}</h3>
                <p>Order Sukses</p>
              </div>
              <div class="icon">
                <i class="ion ion-ios-checkmark"></i>
              </div>
            </div>
          </div>

          <div class="col-md-6">
            <!-- small box -->
            <div class="small-box bg-red">
              <div class="inner text-center">
                <h3>{{ format_rupiah($count_total_penjualan) }}</h3>
                <p>Total Penjualan</p>
              </div>
              <div class="icon">
                <i class="ion ion-cash"></i>
              </div>
            </div>
          </div>



        </div>
        <div class="row">
            <div class="col-md-6">
            {!! $chart_order->html() !!}
            </div>
            <div class="col-md-6">
            {!! $kategori_produk->html() !!}
            </div>
        </div>


        <!-- /.box-body -->
        <div class="box-footer">
        &nbsp;
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
</div>

{!! Charts::scripts() !!}
{!! $chart_order->script() !!}
{!! $kategori_produk->script() !!}

@endsection
