
  @extends('layouts.utama')
  @section('content')
  <script type="text/javascript">
    $(document).ready(function() {

        $('.dateinput').datepicker({
            format: "yyyy/mm/dd",
            autoclose: true,
            // endDate: "+0d",
            yearRange: "yy-50:yy+1",
        });

        $('#sku').prop('readonly',true);
        $('#harga1').val(formatRupiah($('#harga1').val(), 'Rp. '));
        toRupiah('harga1','harga');

  });
  </script>
  <section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Produk Edit Data</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>

      <div class="box-body">
        <div class="col-md-12">
          <form method="POST" action="{{action('ProdukController@update', $id)}}" enctype="multipart/form-data">
            @csrf
            <input name="_method" type="hidden" value="PATCH">

            @if ($errors->any())
              <div class="alert alert-danger">
                <ul>
                  @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                  @endforeach
                </ul>
              </div><br />
            @endif
          <input type="hidden" name="produk_id" id="produk_id" value="{{ $id }}">
          <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">General</a></li>
                {{-- <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Atribut</a></li> --}}
                <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">Gambar Produk</a></li>
                {{-- <li class=""><a href="#tab_4" data-toggle="tab" aria-expanded="false">Harga Spesifik</a></li> --}}


              </ul>
              <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
                      <div class="form-group {{ $errors->has('nama') ? $class_error : "" }}">
                          <label>Nama </label>
                          <input name="nama" class="form-control" id="nama" value="{{ $errors->has('nama') ? "" : $produk['nama'] }}" />
                          <small class="text-danger">{{ $errors->first('nama') }}</small>
                      </div>
                      <div class="form-group {{ $errors->has('kategori_id') ? $class_error : "" }}">
                        <label>Kategori</label>
                        <select class="form-control" name="kategori_id">
                            @foreach ($kategoris as $val)
                            <option value="{{ $val['id'] }}" @if($produk['kategori_id']==$val['id']) selected @endif>{{$val['nama']}}</option>
                            @endforeach
                        </select>
                        <small class="text-danger">{{ $errors->first('kategori_id') }}</small>
                      </div>
                      <div class="form-group {{ $errors->has('deskripsi') ? $class_error : "" }}">
                          <label>Deskripsi</label>
                          <textarea name="deskripsi" class="form-control" id="deskripsi" rows="20">{{ $errors->has('deskripsi') ? "" : $produk['deskripsi'] }}</textarea>
                          <small class="text-danger">{{ $errors->first('deskripsi') }}</small>
                      </div>
                      <div class="form-group {{ $errors->has('sku') ? $class_error : "" }}">
                          <label>SKU </label>
                          <input name="sku" class="form-control" id="sku" value="{{ $produk['sku'] }}" />
                          <small class="text-danger">{{ $errors->first('sku') }}</small>
                      </div>
                      <div class="form-group {{ $errors->has('stok') ? $class_error : "" }}">
                          <label>Stok </label>
                          <input name="stok" type="number" class="form-control" id="stok" value="{{ $errors->has('stok') ? "" : $produk['stok'] }}" />
                          <small class="text-danger">{{ $errors->first('stok') }}</small>
                      </div>
                      <div class="form-group {{ $errors->has('harga') ? $class_error : "" }}">
                          <label>Harga </label>
                          <input name="harga1" type="text" class="form-control" id="harga1" value="{{ $errors->has('harga') ? "" : $produk['harga'] }}" />
                          <input type="hidden" name="harga" id="harga" value="{{ $produk['harga'] }}">
                          <small class="text-danger">{{ $errors->first('harga') }}</small>
                      </div>
                      <div class="form-group {{ $errors->has('active') ? $class_error : "" }}">
                          <label>Status </label>
                          <select class="form-control" name="active" >
                              <option value="">--Pilih--</option>
                              <option value=0 @if($produk['active']==0) selected @endif>Inactive</option>
                              <option value=1 @if($produk['active']==1) selected @endif>Active</option>
                          </select>
                          <small class="text-danger">{{ $errors->first('active') }}</small>
                      </div>
                  </div>

                  {{-- <div class="tab-pane" id="tab_2">
                      <div class="col-md-12 well" style="margin: 15px 0px !important;"><h3 style="margin: 0;">Clothes</h3></div>
                  </div> --}}

                  <div class="tab-pane" id="tab_3">
                      <div class="col-md-12">
                            <div class="dropzone dz-clickable sortable" >
                                <div class="dz-message">
                                    Drop files here or click to upload.
                                </div>

                                    @foreach($produk_gambars as $produk_gambar)
                                        <div class="dz-preview" data-id="{{ $produk_gambar->id }}" >
                                            <img class="dropzone-thumbnail" src={{ asset('uploads/products/'.$produk_gambar->nama) }} height="120px" width="120px">
                                            <a class="dz-remove" href="javascript:void(0);" data-remove="{{ $produk_gambar->id }}">Remove file</a>
                                        </div>
                                    @endforeach

                            </div>
                            <br>
                            <small>maximum <b>4</b> files</small>
                      </div>


                  </div>
                    <!-- /.tab-pane -->
                  {{-- <div class="tab-pane" id="tab_4">
                      <div class="form-group {{ $errors->has('tipe_diskon') ? $class_error : "" }}">
                          <label>Tipe Diskon  </label>
                          <select class="form-control" name="tipe_diskon" >
                              <option value="">--Tipe--</option>
                              <option value="angka" @if(old('tipe_diskon')=='angka') selected @endif>Angka</option>
                              <option value="harga" @if(old('tipe_diskon')=='harga') selected @endif>Harga</option>
                          </select>
                          <small class="text-danger">{{ $errors->first('jk') }}</small>
                      </div>
                      <div class="form-group {{ $errors->has('reduksi') ? $class_error : "" }}">
                          <label>Reduksi </label>
                          <input name="reduksi" class="form-control" id="reduksi" value="{{ old('reduksi')}}" />
                          <small class="text-danger">{{ $errors->first('reduksi') }}</small>
                      </div>

                      <div class="form-group {{ $errors->has('tanggal_mulai') ? $class_error : "" }}">
                          <label>Tanggal Mulai</label>

                          <div class="input-group date">
                            <div class="input-group-addon">
                              <i class="fa fa-calendar"></i>
                            </div>
                            <input name="tanggal_mulai" type="text" class="form-control pull-right dateinput" id="tanggal_mulai" value="{{ old('tanggal_mulai') }}">
                          </div>
                          <!-- /.input group -->
                          <small class="text-danger">{{ $errors->first('tanggal_mulai') }}</small>
                      </div>
                      <div class="form-group {{ $errors->has('tanggal_selesai') ? $class_error : "" }}">
                          <label>Tanggal Selesai</label>

                          <div class="input-group date">
                            <div class="input-group-addon">
                              <i class="fa fa-calendar"></i>
                            </div>
                            <input name="tanggal_selesai" type="text" class="form-control pull-right dateinput" id="tanggal_selesai" value="{{ old('tanggal_selesai') }}">
                          </div>
                          <!-- /.input group -->
                          <small class="text-danger">{{ $errors->first('tanggal_selesai') }}</small>
                      </div>
                  </div> --}}
                    <!-- /.tab-pane -->
              </div>
                  <!-- /.tab-content -->
          </div>
        </div>
      </div>
                <!-- /.tab-pane -->


        <div class="box-footer">
          <div class="col-md-6">
            <div class="form-group">
              <button type="submit" id="submit" class="btn btn-primary">Submit</button>
            </div>
          </div>
        </div>
        </form>
      </div>

      <link rel="stylesheet" href="{{ asset('css/dropzone.css') }}">
      <script src="{{ asset('js/dropzone.js') }}"></script>



      <script type="text/javascript">



        Dropzone.autoDiscover = false;

          var uploaded = false;

          var dropzone = new Dropzone(".dropzone", {
            url: "{{ route('uploadimage')  }}",


            // autoProcessQueue: false,
            uploadMultiple: true,
            parallelUploads: 4,
            maxFiles: 4,
            maxFilesize: 5,
            acceptedFiles: ".jpeg,.jpg,.png,.gif",
            dictFileTooBig: 'Image is bigger than 5MB',
            addRemoveLinks: true,

            // previewTemplate:
            sending: function(file, xhr, formData) {
                formData.append("_token", $('[name=_token').val());
                formData.append("produk_id", $('#produk_id').val());
            },
            error: function(file, response) {

                  $(file.previewElement).find('.dz-error-message').remove();
                  $(file.previewElement).remove();

                  $(function(){
                    new PNotify({
                      title: file.name+" gagal di upload!",
                      text: response,
                      type: "error",
                      icon: false

                    });
                  });

              },
              // previewsContainer: null,
              // hiddenInputContainer: "body",

              success : function(file, images){
                $('.dropzone').empty();
                $.each(images, function(index, val) {
                  $('.dropzone').append('<div class="dz-preview" data-id="'+val.id+'"><img class="dropzone-thumbnail" src="{{ asset('uploads/products') }}/'+val.nama+'" height="120px" width="120px" /><a class="dz-remove" href="javascript:vo(0);" data-remove="'+val.id+'">Remove file</a></div>');

                });

                $(function(){
                  new PNotify({
                    title: false,
                    text: 'Produk Gambar sukses di upload',
                    type: "success",
                    icon: false,


                  });
                });

              }
			    });

         // Delete image
			$(document).on('click', '.dz-remove', function () {
				var id = $(this).data('remove');

				$.ajax({
					url: "{{ route('removeimage') }}",
                    type: 'POST',
                    headers: {
                            'X-CSRF-TOKEN': '{!! csrf_token() !!}'
                        },
                    dataType: 'json',
                    data: {
                            id: id
                        },
                    })
                    .done(function(status) {
                        var notification_type;

                        if (status.success) {
                            notification_type = 'success';
                            $('div.dz-preview[data-id="'+id+'"]').remove();
                        } else {
                            notification_type = 'error';
                        }

                        $(function(){
                        new PNotify({
                        text: status.message,
                        type: notification_type,
                        icon: false
                        });
                    });
				});

      });
    </script>


      <style>
          .dropzone {
              border: 2px dashed #0087F7;
              border-radius: 5px;
              background: white;
          }
      </style>

@endsection




