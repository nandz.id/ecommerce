@extends('layouts.utama')
@section('content')
<script type="text/javascript">
  $(document).ready(function() {

    $('.dateinput').datepicker({
      format: "yyyy/mm/dd",
      autoclose: true,
      // endDate: "+0d",
      yearRange: "yy-50:yy+1",
    });

    $('#sku').prop('readonly',true);

    toRupiah('harga1','harga');

});

    /* Dengan Rupiah */





</script>
<section class="content">
    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Produk Tambah Data</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                  title="Collapse">
            <i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fa fa-times"></i></button>
        </div>
      </div>

    <div class="box-body">
      <div class="col-md-12">
        <form id="frmTarget" method="post" action="{{url('produk')}}" enctype="multipart/form-data">
          @csrf
          @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div><br />
          @endif
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true">General</a></li>
              {{-- <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Atribut</a></li> --}}
              <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">Gambar Produk</a></li>
              {{-- <li class=""><a href="#tab_4" data-toggle="tab" aria-expanded="false">Harga Spesifik</a></li> --}}


            </ul>
        <input type="hidden" name="produk_id" id="produk_id" value="{{ ($produk_id->id + 1) }}">
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                    <div class="form-group {{ $errors->has('nama') ? $class_error : "" }}">
                        <label>Nama </label>
                        <input name="nama" class="form-control" id="nama" value="{{ old('nama')}}" />
                        <small class="text-danger">{{ $errors->first('nama') }}</small>
                    </div>

                    <div class="form-group {{ $errors->has('kategori_id') ? $class_error : "" }}">
                        <label>Kategori</label>
                        <select class="form-control" name="kategori_id">
                            <option value="">--Pilih--</option>
                            @foreach ($kategoris as $val)
                                <option value="{{ $val->id }}" @if(old('kategori_id')==$val['id']) selected @endif> {{ $val->nama }} </option>
                            @endforeach
                        </select>
                        <small class="text-danger">{{ $errors->first('kategori_id') }}</small>
                    </div>

                    <div class="form-group {{ $errors->has('deskripsi') ? $class_error : "" }}">
                        <label>Deskripsi</label>
                        <textarea name="deskripsi" class="form-control" id="deskripsi" rows="20">{{ old('deskripsi') }}</textarea>
                        <small class="text-danger">{{ $errors->first('deskripsi') }}</small>
                    </div>
                    <div class="form-group {{ $errors->has('sku') ? $class_error : "" }}">
                        <label>SKU </label>
                        <input name="sku" class="form-control" id="sku" value="{{ old('sku') ?? $sku }}" />
                        <small class="text-danger">{{ $errors->first('sku') }}</small>
                    </div>
                    <div class="form-group {{ $errors->has('stok') ? $class_error : "" }}">
                        <label>Stok </label>
                        <input name="stok" type="number" class="form-control" id="stok" value="{{ old('stok') }}" />
                        <small class="text-danger">{{ $errors->first('stok') }}</small>
                    </div>
                    <div class="form-group {{ $errors->has('price') ? $class_error : "" }}">
                        <label>Harga </label>
                        <input name="harga1" type="text" class="form-control" id="harga1" value="{{ old('harga') }}" />
                        <input type="hidden" name="harga" id="harga">
                        <small class="text-danger">{{ $errors->first('harga') }}</small>
                    </div>
                    <div class="form-group {{ $errors->has('active') ? $class_error : "" }}">
                        <label>Status </label>
                        <select class="form-control" name="active" >
                            <option value="">--Pilih--</option>
                            <option value=0 @if(old('active')==0) selected @endif>Inactive</option>
                            <option value=1 @if(old('active')==1) selected @endif>Active</option>
                        </select>
                        <small class="text-danger">{{ $errors->first('jk') }}</small>
                    </div>
                </div>

                {{-- <div class="tab-pane" id="tab_2">
                    <div class="col-md-12 well" style="margin: 15px 0px !important;"><h3 style="margin: 0;">Clothes</h3></div>
                </div> --}}

                <div class="tab-pane" id="tab_3">
                    <div class="col-md-12">
                        <div class="dropzone" id="my-dropzone" name="myDropzone">

                        </div>
                    </div>

                </div>
                  <!-- /.tab-pane -->
                {{-- <div class="tab-pane" id="tab_4">
                    <div class="form-group {{ $errors->has('tipe_diskon') ? $class_error : "" }}">
                        <label>Tipe Diskon  </label>
                        <select class="form-control" name="status" >
                            <option value="">--Tipe--</option>
                            <option value="angka" @if(old('tipe_diskon')=='angka') selected @endif>Angka</option>
                            <option value="harga" @if(old('tipe_diskon')=='harga') selected @endif>Harga</option>
                        </select>
                        <small class="text-danger">{{ $errors->first('jk') }}</small>
                    </div>
                    <div class="form-group {{ $errors->has('reduksi') ? $class_error : "" }}">
                        <label>Reduksi </label>
                        <input name="reduksi" class="form-control" id="reduksi" value="{{ old('reduksi')}}" />
                        <small class="text-danger">{{ $errors->first('reduksi') }}</small>
                    </div>

                    <div class="form-group {{ $errors->has('tanggal_mulai') ? $class_error : "" }}">
                        <label>Tanggal Mulai</label>

                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input name="tanggal_mulai" type="text" class="form-control pull-right dateinput" id="tanggal_mulai" value="{{ old('tanggal_mulai') }}">
                        </div>
                        <!-- /.input group -->
                        <small class="text-danger">{{ $errors->first('tanggal_mulai') }}</small>
                    </div>
                    <div class="form-group {{ $errors->has('tanggal_selesai') ? $class_error : "" }}">
                        <label>Tanggal Selesai</label>

                        <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          <input name="tanggal_selesai" type="text" class="form-control pull-right dateinput" id="tanggal_selesai" value="{{ old('tanggal_selesai') }}">
                        </div>
                        <!-- /.input group -->
                        <small class="text-danger">{{ $errors->first('tanggal_selesai') }}</small>
                    </div>
                </div> --}}
                  <!-- /.tab-pane -->
            </div>
                <!-- /.tab-content -->
        </div>
      </div>
    </div>
              <!-- /.tab-pane -->


      <div class="box-footer">
        <div class="col-md-6">
          <div class="form-group">
            <button type="submit" id="submit" class="btn btn-primary">Submit</button>
          </div>
        </div>
      </div>
      </form>
    </div>



    <link rel="stylesheet" href="{{ asset('css/dropzone.css') }}">
    <script src="{{ asset('js/dropzone.js') }}"></script>


    <script type="text/javascript">

       Dropzone.options.myDropzone= {
           url: "{{ route('uploadimage') }}",
           headers: {
               'X-CSRF-TOKEN': '{!! csrf_token() !!}'
           },
           autoProcessQueue: false,
           uploadMultiple: true,
           parallelUploads: 4,
           maxFiles: 4,
           maxFilesize: 5,
           acceptedFiles: ".jpeg,.jpg,.png,.gif",
           dictFileTooBig: 'Image is bigger than 5MB',
           addRemoveLinks: true,

            init: function () {

            var myDropzone = this;

            // Update selector to match your button
            $("#submit").click(function (e) {
                // e.preventDefault();
                myDropzone.processQueue();



            });

            this.on('sending', function(file, xhr, formData) {
                // Append all form inputs to the formData Dropzone will POST
                formData.append("_token", $('[name=_token').val());
                formData.append("produk_id", $('#produk_id').val());

            });
            },
                removedfile: function(file)
                {
                    var name = file.upload.filename;
                    $.ajax({
                        headers: {
                                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                                },
                        type: 'POST',
                        url: "{{ route('removeimage') }}",
                        data: {filename: name},
                        success: function (data){
                            console.log("File has been successfully removed!!");
                        },
                        error: function(e) {
                            console.log(e);
                        }});
                        var fileRef;
                        return (fileRef = file.previewElement) != null ?
                        fileRef.parentNode.removeChild(file.previewElement) : void 0;
                },
            previewsContainer: null,
            hiddenInputContainer: "body",
       }
    </script>
    <style>
        .dropzone {
            border: 2px dashed #0087F7;
            border-radius: 5px;
            background: white;
        }
    </style>
@endsection


