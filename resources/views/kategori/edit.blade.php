@extends('layouts.utama')
@section('content')
<script type="text/javascript">

 $(document).ready(function() {
     $('#to_slug').prop('readonly',true);
 });

function convertToSlug( str ) {
    //replace all special characters | symbols with a space
    str = str.replace(/[`~!@#$%^&*()_\-+=\[\]{};:'"\\|\/,.<>?\s]/g, ' ').toLowerCase();

    // trim spaces at start and end of string
    str = str.replace(/^\s+|\s+$/gm,'');

    // replace space with dash/hyphen
    str = str.replace(/\s+/g, '-');
    $('#to_slug').val(str);
    //return str;
}

</script>

<section class="content">
    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Kategori Edit Data</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                  title="Collapse">
            <i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fa fa-times"></i></button>
        </div>
      </div>

    <div class="box-body">
      <div class="col-md-6">
        <form method="POST" action="{{action('KategoriController@update', $id)}}">
          @csrf
          <input name="_method" type="hidden" value="PATCH">
          <div class="form-group {{ $errors->has('nama') ? $class_error : "" }}">
            <label>Nama Kategori </label>
            <input name="nama" class="form-control" id="nama" value="{{ $errors->has('nama') ? "" : $kategori['nama'] }}"  onload="convertToSlug(this.value)" onkeyup="convertToSlug(this.value)">
            <small class="text-danger">{{ $errors->first('nama') }}</small>
          </div>

          <div class="form-group {{ $errors->has('slug') ? $class_error : "" }}">
            <label>Nama Slug </label>
            <input name="slug" class="form-control" id="to_slug" value="{{ $errors->has('slug') ? "" : $kategori['slug'] }}">
            <small class="text-danger">{{ $errors->first('slug') }}</small>
          </div>

          <div class="form-group {{ $errors->has('keterangan') ? $class_error : "" }}">
            <label>Keterangan (Optional)</label>
            <textarea name="keterangan" class="form-control" id="keterangan" rows="6">{{ $errors->has('keterangan') ? "" : $kategori['keterangan'] }}</textarea>
          </div>
          <small class="text-danger">{{ $errors->first('keterangan') }}</small>
        </div>
      </div>
      <div class="box-footer">
        <div class="col-md-6">
          <div class="form-group">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </div>
      </div>
      </form>
    </div>
@endsection
