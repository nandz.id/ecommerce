@extends('layouts.utama')
@section('content')

<script type="text/javascript">
  $(document).ready(function(){
    $('#example').dataTable({
      "bPaginate": true,
      "bInfo": true,
      "bSort": true
    });
  $('#alert_success').fadeTo(2000, 500).slideUp(500, function(){ $("#success-alert").slideUp(500);});
  });

  $(document).on('click', '.klik', function () {
        swal({
            title: "Apakah anda yakin row ini akan dihapus?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                var id = $(this).attr('value-id');
                $('#hapus_'+id+'').trigger('click');
                swal("Row Telah dihapus !", {
                icon: "success",
                });
            } else {
                swal("Tidak Jadi dihapus.");
            }
        });

    });

</script>
    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Jabatan</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>

      <div class="box-body">
        @if (\Session::has('success'))
          <div class="alert alert-success alert-dismissible" id="alert_success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="fa fa-check"></i> {{ \Session::get('success') }} !</h5>
          </div>
        @endif
        <div class="form-group">
          <a href="{{ url('divisi/create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>
        </div>

        <table id="example" class="table table-bordered table-striped table-condensed">
        <thead>
          <tr>
            <th width="5%">No. </th>
            <th width="35%">Nama Divisi</th>
            <th width="35%">Keterangan</th>
            <th width="15%">Action</th>
          </tr>
        </thead>
        <tbody>
          @php ($no = 1)
          @foreach($divisi as $val)
            <tr>
              <td>{{$no}}</td>
              <td>{{$val->nama_divisi}}</td>
              <td>{{$val->keterangan}}</td>

              <td align="center">
                <div class="row">
                    <div class="col-md-6 text-right" style="padding-left:0px;padding-right:0px">
                         <form action="{{action('ProdukController@destroy', $val->id)}}" method="post">
                            <a href="{{action('ProdukController@edit', $val->id)}}" class="btn btn-sm btn-success" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
                            @csrf
                            <input name="_method" type="hidden" value="DELETE">
                            <input type="submit" name="submit" class="submit" id="hapus_{{$val->id}}" style="display:none;">
                        </form>
                    </div>
                    <div class="col-md-6 text-left" style="padding-left:0px;padding-right:0px">
                         <button class="btn btn-sm btn-danger klik" data-toggle="tooltip" value-id ="{{ $val->id }} "title="Edit"> <i class="fa fa-trash"></i></button>
                    </div>
                </div>
            </tr>
            @php ($no++)
          @endforeach
        </tbody>
      </table>
      {{-- {{ $karyawan->onEachSide(1)->links() }} --}}
      </div>
    </div>
@endsection
