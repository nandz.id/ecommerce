@extends('layouts.utama')
@section('content')
<section class="content">
    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Divisi Tambah Data</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                  title="Collapse">
            <i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fa fa-times"></i></button>
        </div>
      </div>

    <div class="box-body">
      <div class="col-md-6">
        <form method="post" action="{{url('divisi')}}" enctype="multipart/form-data">
          @csrf
          @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div><br />
          @endif
          <div class="form-group {{ $errors->has('nama_divisi') ? $class_error : "" }}">
            <label>Nama Divisi </label>
          <input name="nama_divisi" class="form-control" id="nama_divisi" value="{{ old('nama_divisi') }}" />
          <small class="text-danger">{{ $errors->first('nama_divisi') }}</small>
          </div>

          <div class="form-group {{ $errors->has('keterangan') ? $class_error : "" }}">
            <label>Keterangan</label>
            <textarea name="keterangan" class="form-control" id="keterangan" rows="6">{{ old('keterangan') }}</textarea>
            <small class="text-danger">{{ $errors->first('keterangan') }}</small>
          </div>
        </div>
      </div>
      <div class="box-footer">
        <div class="col-md-6">
          <div class="form-group">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </div>
      </div>
      </form>
    </div>
@endsection
