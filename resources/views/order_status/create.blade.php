@extends('layouts.utama')
@section('content')

<section class="content">
    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Order Status Tambah Data</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                  title="Collapse">
            <i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fa fa-times"></i></button>
        </div>
      </div>

    <div class="box-body">
      <div class="col-md-6">
        <form method="post" action="{{url('order_status')}}" enctype="multipart/form-data">
          @csrf
          @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div><br />
          @endif
          <div class="form-group {{ $errors->has('nama') ? $class_error : "" }}">
            <label>Nama Order Status</label>
          <input name="nama" class="form-control" id="nama" value="{{ old('nama') }}" />
          <small class="text-danger">{{ $errors->first('nama') }}</small>
          </div>

          <div class="form-group {{ $errors->has('notifikasi') ? $class_error : "" }}">
            <label>Notifikasi </label>
            <select class="form-control" name="notifikasi" >
                <option value="">--Pilih--</option>
                <option value='1' @if(old('notifikasi')=='1') selected @endif>Yes</option>
                <option value='0' @if(old('notifikasi')=='0') selected @endif>No</option>
            </select>
            <small class="text-danger">{{ $errors->first('notifikasi') }}</small>
          </div>
        </div>
      </div>
      <div class="box-footer">
        <div class="col-md-6">
          <div class="form-group">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </div>
      </div>
      </form>
    </div>

@endsection
