@extends('layouts.utama')
@section('content')
<script type="text/javascript">
  $(document).ready(function() {

    $('.tanggal_input').datepicker({
      format: "yyyay/mm/dd",
      autoclose: true,
      // endDate: "+0d",
      yearRange: "yy-50:yy+1",
    });

    $('#nik').prop('readonly',true);

});
</script>

<section class="content">

	<div class="row">
		<div class="col-md-12 well">
			<h2>Order #{{ $orders->order_kode }} - Customer</h2>
		</div>
    </div>
     <?php
        $class="";
        if($orders->order_status_id == 1) {
            $class_span="default";
        }
        elseif($orders->order_status_id == 2) {
            $class_span="warning";
        }
        elseif($orders->order_status_id == 3) {
            $class_span="success";
        }
        else {
            $class_span="info";
        }
    ?>

	<div class="row">
		<div class="col-md-7">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">
						<span><i class="fa fa-ticket"></i> Order Status</span>
					</h3>
				</div>
				<div class="box-body">
					<h4>
						Current status <br><br>
                    <span class="label label-{{ $class_span }}">{{$orders->order_status->nama}}</span>
					</h4>

					<hr>

					<h4>
						Status history
					</h4>
					<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th>Status</th>
								<th>Tanggal</th>
							</tr>
						</thead>
						<tbody>


                            @foreach($order_status_history as $status_history)
                            <?php
                                $class="";
                                if($status_history->order_status->id == 1) {
                                    $class_span="default";
                                }
                                elseif($status_history->order_status->id == 2) {
                                    $class_span="warning";
                                }
                                elseif($status_history->order_status->id == 3) {
                                    $class_span="success";
                                }
                                else {
                                    $class_span="info";
                                }
                            ?>

							<tr>
								<td><span class="label label-{{$class_span}}">{{ $status_history->order_status->nama }}</span></td>
								<td>{{ $status_history->created_at }}</td>
                            </tr>
                            @endforeach
						</tbody>
					</table>

					<hr>

                    <form action="{{ route('update_order',$id) }}" method="POST">
                        @csrf
                        <input name="_method" type="hidden" value="PATCH">

						<div class="form-group">
							<select name="status_id" id="status_id" class="form-control">
                                @foreach($order_status as $o_status)
                                    <option value="{{ $o_status->id }}" @if($orders->order_status_id==$o_status->id) selected @endif>{{$o_status->nama}}</option>
                                @endforeach
							</select>
						</div>

						<button type="submit" class="btn btn-primary" {{ $orders->order_status_id == 4 ? 'disabled' : '' }}>Update status</button>
					</form>
				</div>
            </div>

			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">
						<span><i class="fa fa-ticket"></i> Status Pembayaran</span>
					</h3>
				</div>
				<div class="box-body">
                    <?php
                        $class="";
                        if($orders->konfirmasi_bayar == 'confirmed' && $orders->status_bayar == 'accepted' ) {
                            $class = 'info';
                        } else {
                            $class = 'warning';
                        }
                    ?>
					<h4>
						Current status <br><br>
                    <span class="label label-{{ $class }}">{{ $orders->status_bayar }}</span>
                    </h4>

                    <table class="table table-condensed table-hover">
                        <tbody>

                            <tr>
                            <td width="30%">Konfirmasi Customer</td>
                            <td width="70%"><span class="label label-{{ $class }}">{{ $orders->konfirmasi_bayar }}</span></td>
                            </tr>
                        </tbody>
                    </table>

					<hr>
					<hr>

                    <form action="{{ route('update_pembayaran',$id) }}" method="POST">

                        @csrf
                        <input name="_method" type="hidden" value="PATCH">

						<div class="form-group">
							<select name="status_bayar" id="status_bayar" class="form-control">
                                <option value="accepted" @if($orders->status_bayar=="acepted") selected @endif>accepted</option>
                                <option value="not accepted" @if($orders->status_bayar=="not accepted") selected @endif>not accepted</option>
							</select>
						</div>

						<button type="submit" class="btn btn-primary" {{ $orders->status_bayar == 'accepted' ? 'disabled' : '' }}>Update status</button>
					</form>
				</div>
			</div>

        </div>

		<div class="col-md-5">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">
						<span><i class="fa fa-user"></i> Customer</span>
					</h3>
				</div>

				<div class="box-body">
					<h3>General</h3>
					<div class="col-md-12 well">
						<div class="col-md-6">
							<i class="fa fa-user-circle-o"></i> Customer <br>
							<i class="fa fa-envelope"></i> <a href="mailto:{{ $orders->email }}">{{ $orders->email }}</a> <br>
						</div>

					</div>
				</div>
			</div>

			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">
						<span><i class="fa fa-user"></i>Pengiriman Kurir</span>
					</h3>
				</div>

				<div class="box-body">
					<div class="nav-tabs-custom">

						<div class="tab-content">
							<div class="tab-pane active" id="tab-shipping-address">
								<h4>Alamat Kirim</h4>
								<table class="table table-condensed table-hover">
									<tbody><tr>
										<td>Nama Customer</td>
										<td>{{ $orders->alamat->nama_depan.' '.$orders->alamat->nama_belakang }}</td>
									</tr>
									<tr>
										<td>Alamat</td>
										<td>
											{{ $orders->alamat->alamat_lengkap }}
										</td>
									</tr>

									<tr>
										<td>Kota</td>
										<td>{{ $orders->alamat->kota }}</td>
									</tr>
									<tr>
										<td>Kode Pos</td>
										<td>{{ $orders->alamat->kode_pos }}</td>
									</tr>
									<tr>
										<td>Handphone</td>
										<td>{{ $orders->alamat->no_handphone }}</td>
									</tr>

								</tbody></table>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>

	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">
						<span><i class="fa fa-truck"></i> Kurir</span>
					</h3>
				</div>
				<div class="box-body">
					<table class="table table-condensed">
						<thead>
							<tr>
								<th style="width: 150px;">Logo</th>
								<th>Nama Kurir</th>
                                <th>Harga</th>
                                <th>Deskripsi</th>
								<th>Lama Pengiriman</th>
							</tr>
						</thead>
						<tbody>
                            <tr>
                                <td class="vertical-align-middle">
                                <img src="{{ Storage::url('img/300/'.$orders->kurir->logo)}}" alt="" style="width: 100px;">
                                </td>
                                <td class="vertical-align-middle">{{ $orders->kurir->nama }}</td>
                                <td class="vertical-align-middle">{{ $orders->kurir->deskripsi }}</td>
                                <td class="vertical-align-middle">{{ format_rupiah($orders->kurir->harga) }}</td>
                                <td class="vertical-align-middle"><span class="label label-success">2 - 3 hari</span></td>
                            </tr>
                        </tbody>
                    </table>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">
						<span><i class="fa fa-shopping-cart"></i> Order Produk</span>
					</h3>
				</div>

				<div class="box-body">
					<div class="col-md-12">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th>Nama Produk</th>
									<th>Harga</th>
									<th>Quantity</th>
									<th class="text-right">Total</th>
								</tr>
							</thead>
							<tbody>
                                @php($total_produk=0)
                                @foreach($order_produks as $order_produk)
                                    <tr>
                                        <td class="vertical-align-middle">{{$order_produk->produk_nama}}</td>
                                        <td class="vertical-align-middle">{{format_rupiah($order_produk->produk_harga)}}</td>
                                        <td class="vertical-align-middle">{{$order_produk->quantity}}</td>
                                        <td class="vertical-align-middle text-right">{{ format_rupiah($total_produk = $order_produk->produk_harga * $order_produk->quantity) }}</td>
                                    </tr>
                                @endforeach
                                <br>
                                    <tr>
                                        <td colspan="3">Total Harga Produk</td>
                                        <td colspan="1" style="text-align: right;">{{ format_rupiah($orders->total) }}</td>
                                    </tr>
							</tbody>
						</table>
					</div>

					<div class="col-md-6 col-md-offset-6">
						<table class="table table-condensed">
							<tbody><tr>
								<td class="text-right">Biaya Pengiriman:</td>
								<td class="text-right">{{ format_rupiah($orders->kurir->harga) }}</td>
							</tr>
							<tr>
								<td class="text-right"><strong>Total Pembayaran:</strong></td>
								<td class="text-right"><strong>{{ format_rupiah($total_pembayaran = $orders->total + $orders->kurir->harga) }}</strong></td>
							</tr>
						</tbody></table>
					</div>

				</div>
			</div>
		</div>
	</div>
</section>

@endsection
