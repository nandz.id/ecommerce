
<style>
        body{
            font-family:'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
            color:#333;
            text-align:left;
            font-size:18px;
            margin:0;
        }
        .container{
            margin:0 auto;
            margin-top:35px;
            padding:40px;
            width:750px;
            height:auto;
            background-color:#fff;
        }
        caption{
            font-size:28px;
            margin-bottom:15px;
        }
        table{
            border:1px solid #333;
            border-collapse:collapse;
            margin:0 auto;
            width:740px;
        }
        td, tr, th{
            padding:12px;
            border:1px solid #333;
            width:185px;
        }
        th{
            background-color: #f0f0f0;
        }
        h4, p{
            margin:0px;
        }
    </style>
<div class="container">
    <table>
        <caption>
            Toko Online XYZ
        </caption>
        <thead>
            <tr>
                <th colspan="3">Invoice <strong>#{{ $orders->invoice_no }}</strong></th>
                <th>{{ $orders->created_at->format('D, d M Y') }}</th>
            </tr>
            <tr>
                <td colspan="2">
                    <h4>Perusahaan: </h4>
                    <p>PT. Toko Online XYZ.<br>
                        Jl ABC<br>
                        085xxxxxxxxxx<br>
                        support@onlinexyz.id
                    </p>
                </td>
                <td colspan="2">
                    <h4>Pelanggan: </h4>
                    <p>{{ $orders->alamat->nama_depan.' '.$orders->alamat->nama_belakang }}<br>
                    {{ $orders->alamat->alamat_lengkap }}<br>
                    {{ $orders->alamat->no_handphone }} <br>
                    {{ $orders->email }}
                    </p>
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>Produk</th>
                <th>Harga</th>
                <th>Qty</th>
                <th>Subtotal</th>
            </tr>

            @foreach ($order_produks as $order_produk)
            <tr>
                <td>{{ $order_produk->produk_nama }}</td>
                <td>{{ format_rupiah($order_produk->produk_harga) }}</td>
                <td>{{ $order_produk->quantity }}</td>
                <td  align="right">{{ format_rupiah($order_produk->quantity * $order_produk->produk_harga ) }}</td>
            </tr>
            @endforeach
            <tr>
                <th colspan="2">Total</th>
                <td> {{ number_format($orders->total_produk) }}</td>
                <td  align="right"> {{ format_rupiah($orders->total) }}</td>

            </tr>
            <tr>
                <th>Nama Kurir</th>
                <th>Lama Kirim</th>
                <th colspan="2">Biaya</th>

            </tr>
            <tr>
                <td> {{ $orders->kurir->nama }} </td>
                <td> <span class="label label-success">2 - 3 hari</span> </td>
                <td colspan="2" align="right"> {{ format_rupiah($orders->kurir->harga) }} </td>
            </tr>


        </tbody>
        <tfoot>
            <tr>
                <th colspan="3">Total Bayar</th>
                <td align="right"> {{ format_rupiah($orders->total + $orders->kurir->harga) }}</td>
            </tr>
        </tfoot>
    </table>
</div>

