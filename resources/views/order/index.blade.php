@extends('layouts.utama')
@section('content')

<script type="text/javascript">
  $(document).ready(function(){
    $('#example').dataTable({
      "bPaginate": true,
      "bInfo": true,
      "bSort": true
    });
  $('#alert_success').fadeTo(2000, 500).slideUp(500, function(){ $("#success-alert").slideUp(500);});
  });

  $(document).on('click', '.klik', function () {
        swal({
            title: "Apakah anda yakin row ini akan dihapus?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                var id = $(this).attr('value-id');
                $('#hapus_'+id+'').trigger('click');
                swal("Row Telah dihapus !", {
                icon: "success",
                });
            } else {
                swal("Tidak Jadi dihapus.");
            }
        });

    });


</script>
    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Order</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>

      <div class="box-body">
        @if (\Session::has('success'))
          <div class="alert alert-success alert-dismissible" id="alert_success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="fa fa-check"></i> {{ \Session::get('success') }} !</h5>
          </div>
        @endif
        {{-- <div class="form-group">
          <a href="{{ url('order/create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>
        </div> --}}

        <table id="example" class="table table-bordered table-striped table-condensed">
        <thead>
          <tr>
            <th width="3%">No. </th>

            <th width="25%">Nama Customer </th>
            <th width="10%">Order Kode</th>
            <th width="10%">Status Order</th>
            <th width="7%">Quantity</th>
            <th width="20%">Total Bayar</th>
            <th width="8%">Konfirmasi bayar</th>
            <th width="10%">Created at</th>
            <th width="10%">Action</th>
          </tr>
        </thead>
        <tbody>

          @php ($no = 1)

          @foreach($orders as $order)
            <?php
                $class_span2='';
                if($order->order_status->id == 1) {
                    $class_span="default";

                }
                elseif($order->order_status->id == 2) {
                    $class_span="warning";
                }
                elseif($order->order_status->id == 3) {
                    $class_span="success";
                }
                else {
                    $class_span="info";

                }
                if($order->status_bayar == 'accepted') {
                    $class_span2="info";
                } else {
                    $class_span2="default";
                }
            ?>

            <tr>
                <td>{{$no}}</td>
                <td>{{$order->alamat->nama_depan.' '.$order->alamat->nama_belakang}}</td>
                <td>{{$order->order_kode}}</td>

                <td><span class="label label-{{$class_span}}">{{ $order->order_status->nama }}</span></td>
                <td>{{$order->total_produk}}</td>
                <td>{{ format_rupiah($order->total)}}</td>
                <td><span class="label label-{{$class_span2}}">{{$order->konfirmasi_bayar}}</span></td>
                <td>{{$order->created_at}}</td>

                <td align="center">
                    <a href="{{action('OrderController@view', $order->id)}}" class="btn btn-sm btn-success" data-toggle="tooltip" title="View"><i class="fa fa-eye"></i></a>
                    <a href="{{ route('orders_print', $order->id) }}" class="btn btn-sm btn-warning" data-toggle="tooltip" title="Print"><i class="fa fa-print"></i></a>
                </td>
            </tr>
          @php ($no ++)
          @endforeach
        </tbody>
      </table>
      {{-- {{ $orders->onEachSide(1)->links() }} --}}
      </div>
    </div>
@endsection
