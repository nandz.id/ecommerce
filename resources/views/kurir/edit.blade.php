@extends('layouts.utama')
@section('content')
<script type="text/javascript">
  $(document).ready(function() {

    $('.tanggal_input').datepicker({
      format: "yyyy/mm/dd",
      autoclose: true,
      // endDate: "+0d",
      yearRange: "yy-50:yy+1",
    });

    $('#nik').prop('readonly',true);
    $('#harga1').val(formatRupiah($('#harga1').val(), 'Rp. '));
    toRupiah('harga1','harga');

});
</script>
<section class="content">
    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Kurir Edit Data</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                  title="Collapse">
            <i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fa fa-times"></i></button>
        </div>
      </div>

    <div class="box-body">
      <div class="col-md-6">
        <form method="POST" action="{{action('KurirController@update', $id)}}" enctype="multipart/form-data">
          @csrf
          @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div><br />
          @endif
          <input name="_method" type="hidden" value="PATCH">

          <div class="form-group {{ $errors->has('nama') ? $class_error : "" }}">
            <label>Nama Kurir </label>
            <input name="nama" class="form-control" id="nama" value="{{ $errors->has('nama') ? "" : $kurir['nama'] }}">
            <small class="text-danger">{{ $errors->first('nama') }}</small>
          </div>
          <div class="form-group {{ $errors->has('harga') ? $class_error : "" }}">
            <label>Harga </label>
            <input name="harga1" type="text" class="form-control" id="harga1" value="{{ $errors->has('harga') ? "" : $kurir['harga'] }}" />
            <input type="hidden" name="harga" id="harga" value="{{ $kurir['harga'] }}">
            <small class="text-danger">{{ $errors->first('harga') }}</small>
          </div>
          <div class="form-group {{ $errors->has('deskripsi') ? $class_error : "" }}">
            <label>Deskripsi </label>
            <textarea name="deskripsi" class="form-control" id="deskripsi" rows="6">{{ $errors->has('deskripsi') ? "" : $kurir['deskripsi'] }}</textarea>
            <small class="text-danger">{{ $errors->first('deskripsi') }}</small>
          </div>
          <div class="form-group {{ $errors->has('logo') ? $class_error : "" }}">
            <label>Logo </label>
            <div class="clearfix" ></div>
            @if ($kurir['logo'] == "")
              <img src="{{ url('img/default-image.png')}}" width="300px" height="300px">
            @else
              <img src="{{ Storage::url('img/500/'.$kurir['logo'])}}" width="300px" height="300px">
            @endif
            <input type="file" name="logo" class="form-control" id="foto" value="" />
            <small class="text-danger">{{ $errors->first('logo') }}</small>
          </div>

        </div>
      </div>
      <div class="box-footer">
        <div class="col-md-6">
          <div class="form-group">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </div>
      </div>
      </form>
    </div>
@endsection
