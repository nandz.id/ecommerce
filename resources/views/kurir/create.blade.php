@extends('layouts.utama')
@section('content')
<script type="text/javascript">
  $(document).ready(function() {

    $('.dateinput').datepicker({
      format: "yyyy/mm/dd",
      autoclose: true,
      // endDate: "+0d",
      yearRange: "yy-50:yy+1",
    });

    $('#nik').prop('readonly',true);
    $('#harga1').val(formatRupiah($('#harga1').val(), 'Rp. '));
    toRupiah('harga1','harga');

});
</script>
<section class="content">
    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Kurir Tambah Data</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                  title="Collapse">
            <i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fa fa-times"></i></button>
        </div>
      </div>

    <div class="box-body">
      <div class="col-md-6">
        <form method="post" action="{{url('kurir')}}" enctype="multipart/form-data">
          @csrf
          @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div><br />
          @endif

          <div class="form-group {{ $errors->has('nama') ? $class_error : "" }}">
            <label>Nama Kurir </label>
          <input name="nama" class="form-control" id="nama" value="{{ old('nama') }}" />
          <small class="text-danger">{{ $errors->first('nama') }}</small>
          </div>
          <div class="form-group {{ $errors->has('price') ? $class_error : "" }}">
            <label>Harga </label>
            <input name="harga1" type="text" class="form-control" id="harga1" value="{{ old('harga') }}" />
            <input name="harga" type="hidden" id="hidden">
            <small class="text-danger">{{ $errors->first('harga') }}</small>
          </div>
          <div class="form-group {{ $errors->has('deskripsi') ? $class_error : "" }}">
            <label>Deskripsi</label>
            <textarea name="deskripsi" class="form-control" id="deskripsi" rows="6">{{ old('deskripsi') }}</textarea>
            <small class="text-danger">{{ $errors->first('deskripsi') }}</small>
          </div>
          <div class="form-group {{ $errors->has('logo') ? $class_error : "" }}">
            <label>Logo </label>
          <input type="file" name="logo" class="form-control" id="logo" value="{{ old('logo') }}" />
          <small class="text-danger">{{ $errors->first('logo') }}</small>
          </div>
        </div>
      </div>
      <div class="box-footer">
        <div class="col-md-6">
          <div class="form-group">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </div>
      </div>
      </form>
    </div>
@endsection
