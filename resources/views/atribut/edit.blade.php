@extends('layouts.utama')
@section('content')
<section class="content">
    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Atribut Edit Data</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                  title="Collapse">
            <i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fa fa-times"></i></button>
        </div>
      </div>

    <div class="box-body">
      <div class="col-md-6">
        <form method="POST" action="{{action('AtributController@update', $id)}}">
          @csrf
          <input name="_method" type="hidden" value="PATCH">
          <div class="form-group {{ $errors->has('nama') ? $class_error : "" }}">
            <label>Nama Atribut </label>
            <input name="nama" class="form-control" id="nama" value="{{ $errors->has('nama') ? "" : $atribut['nama'] }}">
            <small class="text-danger">{{ $errors->first('nama') }}</small>
          </div>

          <div class="form-group {{ $errors->has('tipe') ? $class_error : "" }}">
            <label>Tipe </label>
          <div class="form-group {{ $errors->has('tipe') ? $class_error : "" }}">
            <input name="tipe" class="form-control" id="tipe" value="{{ $errors->has('tipe') ? "" : $atribut['tipe'] }}">
            <small class="text-danger">{{ $errors->first('tipe') }}</small>
          </div>

      </div>
      <div class="box-footer">
        <div class="col-md-6">
          <div class="form-group">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </div>
      </div>
      </form>
    </div>
@endsection
