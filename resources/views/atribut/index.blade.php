@extends('layouts.utama')
@section('content')

<script type="text/javascript">
  $(document).ready(function(){
    $('#example').dataTable({
      "bPaginate": true,
      "bInfo": true,
      "bSort": true
    });
  });

  $(document).on('click', '.klik', function () {
        swal({
            title: "Apakah anda yakin row ini akan dihapus?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
        .then((willDelete) => {
            if (willDelete) {
                var id = $(this).attr('value-id');
                $('#hapus_'+id+'').trigger('click');
                swal("Row Telah dihapus !", {
                icon: "success",
                });
            } else {
                swal("Tidak Jadi dihapus.");
            }
        });

    });

</script>
    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Atribut List</h3>
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>

      <div class="box-body">
        @if (\Session::has('success'))
          <div class="alert alert-success alert-dismissible" id="alert_success">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="fa fa-check"></i> {{ \Session::get('success') }} !</h5>
          </div>
        @endif
        <div class="form-group">
          <a href="{{ url('atribut/create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>
        </div>

        <table id="atribut_list" class="table table-bordered table-striped table-condensed text-centered">
          <thead>
            <tr>
              <th width="5%">No. </th>
              <th width="25%">Nama Atribut</th>
              <th width="25%">Tipe</th>
              <th width="10%">Action</th>
            </tr>
          </thead>
      </table>
      <script type="text/javascript">
        $(document).ready(function() {
          var oTable = $('#atribut_list').DataTable({
            processing: true,
            serverSide: true,
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            ajax: "{{ url('atributlist') }}",
            columns:
            [
              {data: 'DT_RowIndex', name: 'DT_RowIndex'},
              {data: 'nama', name: 'nama'},
              {data: 'tipe', name: 'tipe'},
              {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
          });
        });

        </script>
      {{-- {{ $karyawan->onEachSide(1)->links() }} --}}
      </div>
    </div>
@endsection
