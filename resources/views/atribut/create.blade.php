@extends('layouts.utama')
@section('content')
<script type="text/javascript">
// $(document).ready(function () {
//     $('.tipe_atribut').hide();
// });

</script>

<section class="content">
    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Atribut Tambah Data</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
            <i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fa fa-times"></i></button>
        </div>
      </div>

    <div class="box-body">
      <div class="col-md-6">
        <form method="post" name="add_atribut" id="add_atribut" enctype="multipart/form-data">
          @csrf
          @if ($errors->any())
            <div class="alert alert-danger print-error-msg">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>

                @endforeach
              </ul>
            </div><br />
          @endif
          <div class="form-group {{ $errors->has('nama') ? $class_error : "" }}">
            <label>Nama</label>
          <input name="nama" class="form-control" id="nama" value="{{ old('nama') }}" required/>
          <small class="text-danger">{{ $errors->first('nama') }}</small>
          </div>

            <div class="form-group {{ $errors->has('tipe') ? $class_error : "" }}">
                <label>Tipe </label>
                <select class="form-control" name="tipe" id="attribute_type" >
                    <option value="">--Tipe--</option>
                    <option value="text" @if(old('tipe')=="") selected @endif>Text</option>
                    <option value="textarea" @if(old('tipe')=="textarea") selected @endif>Text Area</option>
                    <option value="dropdown" @if(old('tipe')=="dropdown") selected @endif>Dropdown</option>
                    <option value="multiple_select" @if(old('tipe')=="multiple_select") selected @endif>Multiple Select</option>
                </select>
                <small class="text-danger">{{ $errors->first('tipe') }}</small>
            </div>


            <div class="form-group attr-field attr-type-text col-md-12" style="display: none;">
                <label>Text</label>
                <input type="text" name="text" class="form-control" required/>
            </div>

            <div class="form-group attr-field attr-type-textarea col-md-12" style="display: none;">
                <label>TextArea</label>
                <textarea name="textarea" class="form-control" required/></textarea>
            </div>


            <div class="form-group attr-field attr-type-dropdown col-md-12" style="display: none;">
                <label>Dropdown</label>
                <div class="well">
                    <a href="javascript:void(0)" id="add_option">
                    <i class="fa fa-plus"></i>
                    Add Option
                    </a>
                    <hr>
                    <div class="options">

                    </div>

                </div>
            </div>



      </div>
    </div>
      <div class="box-footer">
        <div class="col-md-6">
          <div class="form-group">
            <button id="submit" type="submit" class="btn btn-primary">Submit</button>
          </div>
        </div>
      </div>
      </form>
    </div>



<script>

    // Reopen attribute specific filed type on redirect back
    $(document).ready(function() {
        var type = $('#attribute_type').val();

        if(type == 'multiple_select') {
        type = 'dropdown';

        $('.attr-type-dropdown').children('label').html('Multiple Select');
        } else {
        $('.attr-type-dropdown').children('label').html('Dropdown');
        }

        $('.attr-type-'+type).show();

        optionNumeration();
    });

    // Option numerotation function
    function optionNumeration() {
    var i = 0;
        $('.option').each(function() {
        i++;
        $(this).find('label').html('Option #'+i);
        $(this).find('input[name^="option"]').attr('name', 'option['+i+']');
        });
    }

    // Add Option
    $(document).on('click', '#add_option', function() {
        // Create option template
        var option_template = '<div class="form-group option {{ $errors->has('option') ? $class_error : "" }} ">'
            option_template +=      '<label>Option</label>'
            option_template +=      '<div class="input-group">'
            option_template +=        '<input type="text" name="option[]" class="form-control" placeholder="Name" required/>'
            option_template +=        '<span class="input-group-addon"><a href="javascript:void(0)" class="remove-option"><i class="fa fa-remove"></i></a></span>'
            option_template +=      '</div>'
            option_template +=  '</div>'

        // Append option
        $('.options').append(option_template);

        // Numerotate options
        optionNumeration();
    });

    // Remove selected option
    $(document).on('click', '.remove-option', function() {
        $(this).closest('.option').remove();

        // Numerotate options
        optionNumeration();
    });

    // Show / Hide attribute specific field type
    $(document).on('change', '#attribute_type', function () {
        var type = $(this).val();

        $('.attr-field').hide();
        $('.options').empty();

        if(type == 'multiple_select') {
        type = 'dropdown';

        $('.attr-type-dropdown').find('label').html('Multiple Select');
        } else {
        $('.attr-type-dropdown').find('label').html('Dropdown');
        }

        $('.attr-type-'+type).show();

    });

    //insert
    $(document).ready(function(){
      var postURL = "<?php echo url('atribut'); ?>";
      var i=1;

      $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });

      $('#submit').click(function(){
           $.ajax({
                url:postURL,
                method:"POST",
                data:$('#add_atribut').serialize(),
                type:'json',
                success:function(data)
                {
                    if(data.error){

                    }else{
                        window.location.replace("<?php echo url('atribut'); ?>");
                        swal("Success!", "Berhasil Menyimpan!", "success");
                    }
                }
           });
      });

      function printErrorMsg (msg) {
         $(".print-error-msg").find("ul").html('');
         $(".print-error-msg").css('display','block');
         $(".print-success-msg").css('display','none');
         $.each( msg, function( key, value ) {
            $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
         });
      }

    });
</script>

@endsection
