@extends('layouts.utama')
@section('content')
<section class="content">
    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Jabatan Edit Data</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                  title="Collapse">
            <i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fa fa-times"></i></button>
        </div>
      </div>

    <div class="box-body">
      <div class="col-md-6">
        <form method="POST" action="{{action('JabatanController@update', $id)}}">
          @csrf
          <input name="_method" type="hidden" value="PATCH">
          <div class="form-group {{ $errors->has('nama_jabatan') ? $class_error : "" }}">
            <label>Nama Jabatan </label>
            <input name="nama_jabatan" class="form-control" id="nama_jabatan" value="{{ $errors->has('nama_jabatan') ? "" : $jabatan['nama_jabatan'] }}">
            <small class="text-danger">{{ $errors->first('nama_jabatan') }}</small>
          </div>
          <div class="form-group {{ $errors->has('divisi_id') ? $class_error : "" }}">
            <label>Nama Divisi</label>
            <select class="form-control" name="divisi_id">
              @foreach ($divisi as $val)
                <option value="{{ $val['id'] }}" @if($jabatan['divisi_id']==$val['id']) selected @endif>{{$val['nama_divisi']}}</option>
              @endforeach
            </select>
            <small class="text-danger">{{ $errors->first('divisi_id') }}</small>
          </div>
          <div class="form-group {{ $errors->has('keterangan') ? $class_error : "" }}">
            <label>Keterangan </label>
            <textarea name="keterangan" class="form-control" id="keterangan" rows="6">{{ $errors->has('keterangan') ? "" : $jabatan['keterangan'] }}</textarea>
          </div>
          <small class="text-danger">{{ $errors->first('keterangan') }}</small>
        </div>
      </div>
      <div class="box-footer">
        <div class="col-md-6">
          <div class="form-group">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </div>
      </div>
      </form>
    </div>
@endsection
