@extends('layouts.utama')
@section('content')

<section class="content">
    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Jabatan Tambah Data</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                  title="Collapse">
            <i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fa fa-times"></i></button>
        </div>
      </div>

    <div class="box-body">
      <div class="col-md-6">
        <form method="post" action="{{url('jabatan')}}" enctype="multipart/form-data">
          @csrf
          @if ($errors->any())
            <div class="alert alert-danger">
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div><br />
          @endif
          <div class="form-group {{ $errors->has('nama_jabatan') ? $class_error : "" }}">
            <label>Nama Jabatan</label>
          <input name="nama_jabatan" class="form-control" id="nama_jabatan" value="{{ old('nama_jabatan') }}" />
          <small class="text-danger">{{ $errors->first('nama_jabatan') }}</small>
          </div>

          <div class="form-group {{ $errors->has('divisi_id') ? $class_error : "" }}">
            <label>Nama Divisi</label>
          <select class="form-control" name="divisi_id">
            <option value="">--Pilih Divisi--</option>
            @foreach($divisi as $val)
              <option value="{{ $val->id }}">{{ $val->nama_divisi}}</option>
            @endforeach
          </select>
          <small class="text-danger">{{ $errors->first('divisi_id') }}</small>
          </div>

          <div class="form-group {{ $errors->has('keterangan') ? $class_error : "" }}">
            <label>Keterangan</label>
            <textarea name="keterangan" class="form-control" id="keterangan" rows="6">{{ old('keterangan') }}</textarea>
            <small class="text-danger">{{ $errors->first('keterangan') }}</small>
          </div>
        </div>
      </div>
      <div class="box-footer">
        <div class="col-md-6">
          <div class="form-group">
            <button type="submit" class="btn btn-primary">Submit</button>
          </div>
        </div>
      </div>
      </form>
    </div>

@endsection
