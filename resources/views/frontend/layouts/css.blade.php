<!-- css.blade.php -->

<link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/styles/bootstrap4/bootstrap.min.css') }}">
<link href="{{ asset('css/frontend/plugins/fontawesome-free-5.0.1/css/fontawesome-all.css') }}" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/plugins/OwlCarousel2-2.2.1/owl.carousel.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/plugins/OwlCarousel2-2.2.1/owl.theme.default.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/plugins/OwlCarousel2-2.2.1/animate.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/plugins/slick-1.8.0/slick.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/styles/main_styles.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/styles/responsive.css') }}">
<!-- Produk -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/styles/product_styles.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/styles/product_responsive.css') }}">

<!-- Shop -->
<link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/styles/shop_styles.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/styles/shop_responsive.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/plugins/jquery-ui-1.12.1.custom/jquery-ui.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/styles/cart_styles.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/styles/cart_responsive.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-spinner.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>






