<!-- Front End -->

<script src="{{asset('js/frontend/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{asset('css/frontend/styles/bootstrap4/popper.js') }}"></script>
<script src="{{asset('css/frontend/styles/bootstrap4/bootstrap.min.js') }}"></script>
<script src="{{asset('css/frontend/plugins/greensock/TweenMax.min.js') }}"></script>
<script src="{{asset('css/frontend/plugins/greensock/TimelineMax.min.js') }}"></script>
<script src="{{asset('css/frontend/plugins/scrollmagic/ScrollMagic.min.js') }}"></script>
<script src="{{asset('css/frontend/plugins/greensock/animation.gsap.min.js') }}"></script>
<script src="{{asset('css/frontend/plugins/greensock/ScrollToPlugin.min.js') }}"></script>
<script src="{{asset('css/frontend/plugins/OwlCarousel2-2.2.1/owl.carousel.js') }}"></script>
<script src="{{asset('css/frontend/plugins/slick-1.8.0/slick.js') }}"></script>
<script src="{{asset('css/frontend/plugins/easing/easing.js') }}"></script>
<script src="{{asset('js/frontend/js/custom.js') }}"></script>

<!-- Produk View -->
<script src="{{asset('js/frontend/js/product_custom.js') }}"></script>

<!-- List Produk -->
<script src="{{asset('css/frontend/plugins/Isotope/isotope.pkgd.min.js') }}"></script>
<script src="{{asset('css/frontend/plugins/jquery-ui-1.12.1.custom/jquery-ui.js') }}"></script>
<script src="{{asset('css/frontend/plugins/parallax-js-master/parallax.min.js') }}"></script>
<script src="{{asset('js/frontend/js/shop_custom.js') }}"></script>
<script src="{{asset('js/frontend/js/cart_custom.js') }}"></script>
<script src="{{asset('js/jquery.spinner.min.js') }}"></script>
<script src="{{asset('js/readmore.min.js') }}"></script>


<script src="{{asset('js/torupiah.js')}}"></script>




