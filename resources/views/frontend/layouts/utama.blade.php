<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>{{ config('app.name', 'Laravel') }}</title>
   <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
 @include('frontend.layouts.css')
</head>
<!-- Site wrapper -->
<body>

<div class="super_container">

    <header>

        @include('frontend.layouts.header')

    </header>

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
     @yield('content')

  </div>
  <!-- /.content-wrapper -->

  @include('frontend.layouts.footer')
</div>
<!-- ./wrapper -->

@include('frontend.layouts.js')
@include('sweet::alert')
</body>
</html>
