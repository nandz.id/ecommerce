<nav class="main_nav">
    <div class="container">
        <div class="row">
            <div class="col">

                <div class="main_nav_content d-flex flex-row">

                    <!-- Categories Menu -->

                    <div class="cat_menu_container">
                        <div class="cat_menu_title d-flex flex-row align-items-center justify-content-start">
                            <div class="cat_burger"><span></span><span></span><span></span></div>
                            <div class="cat_menu_text">categories</div>
                        </div>

                        <ul class="cat_menu">
                            @foreach($kategoris as $kategori)
                                <li><a href="{{ url('frontend/produk-list/'.$kategori['slug'].'') }}">{{ $kategori['nama'] }} <i class="fas fa-chevron-right ml-auto"></i></a></li>
                                {{-- <li><a href="#">Cameras & Photos<i class="fas fa-chevron-right"></i></a></li>
                                <li><a href="#">Smartphones & Tablets<i class="fas fa-chevron-right"></i></a></li>
                                <li><a href="#">TV & Audio<i class="fas fa-chevron-right"></i></a></li>
                                <li><a href="#">Gadgets<i class="fas fa-chevron-right"></i></a></li>
                                <li><a href="#">Car Electronics<i class="fas fa-chevron-right"></i></a></li>
                                <li><a href="#">Video Games & Consoles<i class="fas fa-chevron-right"></i></a></li>
                                <li><a href="#">Accessories<i class="fas fa-chevron-right"></i></a></li> --}}
                            @endforeach
                            <li></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
</nav>

<!-- Menu -->

