
    <!-- Top Bar -->

    <div class="top_bar">
        <div class="container">
            <div class="row">
                <div class="col d-flex flex-row">
                    <div class="top_bar_contact_item"><div class="top_bar_icon"><img src="{{asset('images/phone.png') }}" alt=""></div>+38 068 005 3570</div>
                    <div class="top_bar_contact_item"><div class="top_bar_icon"><img src="{{asset('images/mail.png') }}" alt=""></div><a href="mailto:fastsales@gmail.com">fastsales@gmail.com</a></div>
                    <div class="top_bar_content ml-auto">
                        <div class="top_bar_menu">

                        </div>
                        <div class="top_bar_user">
                            <div class="user_icon"><img src="{{asset('images/user.svg')}}" alt=""></div>
                            <div><a href="#">Register</a></div>
                            <div><a href="#">Sign in</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

