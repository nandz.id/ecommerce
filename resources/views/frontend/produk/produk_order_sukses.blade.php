@extends('frontend.layouts.utama')


@section('content')
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>

<script type="text/javascript">

</script>

<div class="container">
	<div class="row text-center">
        <center>
        <div class="col-sm-6 col-sm-offset-3">
        <br><br> <h2 style="color:#2E7BFF">Order anda telah berhasil !</h2>

        <h4>Terima kasih, {{$orders->nama_depan.' '.$orders->nama_belakang}} atas pembelian produk di website kami !</h4>
        <p style="font-size:20px;color:#5C5C5C;">Segera kirim pembayaran ke Rekening BCA, AN: X No. Rekening : 01234567889</p>
        <br>
        <p style="font-size:20px;color:#5C5C5C;">Kami telah mengirim detail order dan konfirmasi pembayaran ke alamat email anda {{ $orders->email }} </p>
        <a href="{{url('/frontend/produk-list')}}" class="btn btn-success">Lanjut Belanja</a>
    <br><br>
        </div>
        </center>

	</div>
</div>
<!-- akhir baris kedua -->
@endsection
