@extends('frontend.layouts.utama')
@section('content')

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
<script type="text/javascript">

    $(document).ready(function() {
        $("html, body").delay(100).animate({scrollTop: $('.single_product').offset().top }, 1000);

        details();

        $('.cart_button').click(function () {

            add_cart();
            details();
            swal("Success!", "Produk berhasil dimasukkan ke Keranjang !", "success");
        })

        $('article').readmore({
            speed: 100,
            collapsedHeight: 200,
            heightMargin: 16,
            moreLink: '<a href="#">Read More</a>',
            lessLink: '<a href="#">Close</a>',
            embedCSS: true,
            blockCSS: 'display: none; width: 100%;',
            startOpen: false,
            // callbacks
            blockProcessed: function() {},

            beforeToggle: function(){},

            afterToggle: function(){}
        });

    })





    function add_cart() {

        $.ajax({
            headers: {
               'X-CSRF-TOKEN': '{!! csrf_token() !!}'
            },
            url:"{{ route('add_cart') }}",
            method:'POST',
            data:{
                id    : $('#id').val(),
                nama  : $('#nama_produk').val(),
                harga : $('#harga').val(),
                qty   : $('.qty').val(),
                gambar: $('#gambar').val()
                    },
            dataType:'json',

            success:function(data)
            {
                console.log(data);

            }

        })
    }





</script>

    @include('frontend.produk.produk_header')
    @include('frontend.produk.produk_menu')

    <!-- Single Product -->
    <div class="single_product">
            <div class="container">
                <div class="row">

                    <!-- Images -->
                    <div class="col-lg-2 order-lg-1 order-2">
                        <ul class="image_list">
                            @foreach($produk_gambars as $produk_gambar)
                                <li data-image="{{ asset('uploads/products/'.$produk_gambar['nama'].'') }}"><img src="{{ asset('uploads/products/'.$produk_gambar['nama'].'') }}" alt=""></li>
                            @endforeach
                        </ul>
                    </div>

                    <!-- Selected Image -->
                    <div class="col-lg-5 order-lg-2 order-1">
                    <input type="hidden" id="id" value="{{ $produk_details->id }}">
                        <input type="hidden" id="gambar" value="{{ $gambar_selected }}">
                        <div class="image_selected"><img src="{{ asset('uploads/products/'.$gambar_selected.'') }}" alt=""></div>
                    </div>

                    <!-- Description -->
                    <div class="col-lg-5 order-3">
                        <div class="product_description">
                            <div class="product_category">
                                <label class=''>{{ $produk_details->nama_kategori }}</label><br>
                                <label>{{ $produk_details->sku }}</label><br>
                                @if($produk_details->stok > 0)
                                    <h3><span class="badge badge-success">Instock</span></h3>
                                @else
                                    <h3><span class="badge badge-danger">Stok Habis</span></h3>
                                @endif


                            </div>

                            <div class="product_name">{{ $produk_details->nama }}
                                <input type="hidden" id="nama_produk" value="{{ $produk_details->nama }}">
                            </div>

                            {{-- <div class="rating_r rating_r_4 product_rating"><i class="fas fa-chevron-up"></i><i></i><i></i><i></i><i></i></div> --}}
                            <div class="product_text">

                                {!! $produk_details->deskripsi !!}


                            </div>
                            <div class="order_info d-flex flex-row">
                                <form action="#">
                                    <div class="clearfix" style="z-index: 1000;">

                                        <!-- Product Quantity -->
                                        <div class="product_quantity clearfix">
                                            <span>Quantity: </span>
                                            <input id="quantity_input" class="qty" type="text" pattern="[0-9]*" value=1>
                                            <div class="quantity_buttons">
                                                <div id="quantity_inc_button" class="quantity_inc quantity_control"><i class="fas fa-chevron-up"></i></div>
                                                <div id="quantity_dec_button" class="quantity_dec quantity_control"><i class="fas fa-chevron-down"></i></div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="product_price" style="font-size:20px;">
                                        {{ format_rupiah($produk_details->harga) }}
                                        <input type="hidden" id="harga" value="{{ $produk_details->harga }}">
                                    </div>

                                    <div class="button_container">
                                        <input type="hidden" id="id_produk" value="{{ $produk_details->id }}">


                                        <button type="button" class="button cart_button" id="submit">Add to Cart</button>
                                        <div class="product_fav"><i class="fas fa-heart"></i></div>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>



@endsection
