<script type="text/javascript">
    $(document).ready(function() {
        details();
    });

    $(".click_cart").click(function() {
        window.location = $(this).find("a").attr("href");
        return false;
    });

    function details() {

        $.ajax({
            headers: {
               'X-CSRF-TOKEN': '{!! csrf_token() !!}'
            },
            url:"{{ route('details_cart') }}",
            method:'GET',
            data:{
                    },
            dataType:'json',

            success:function(data)
            {
                $('.cart_count').html(data.data.total_quantity);
                $('.cart_price').html('Rp. '+formatNumber(data.data.total));

            }
        })
    }

    function formatNumber(num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }

</script>

<div class="header_main">
    <div class="container">
        <div class="row">

            <!-- Logo -->
            <div class="col-lg-2 col-sm-3 col-3 order-1">
                <div class="logo_container">
                    <div class="logo"><a href="#">OneTech</a></div>
                </div>
            </div>



            <!-- Wishlist -->
            <div class="col-lg-4 col-9 order-lg-3 order-2 text-lg-left text-right">
                <div class="wishlist_cart d-flex flex-row align-items-center justify-content-end">
                    {{-- <div class="wishlist d-flex flex-row align-items-center justify-content-end">
                        <div class="wishlist_icon"><img src="{{ asset('images/heart.png') }}" alt=""></div>
                        <div class="wishlist_content">
                            <div class="wishlist_text"><a href="#">Wishlist</a></div>
                            <div class="wishlist_count">115</div>
                        </div>
                    </div> --}}

                    <!-- Cart -->
                    <div class="click_cart">

                        <div class="cart">
                             <a href="{{ url('frontend/produk-cart') }}" tabindex="0">
                            <div class="cart_container d-flex flex-row align-items-center justify-content-end">
                                <div class="cart_icon">
                                    <img src="{{ asset('images/cart.png') }}" alt="">

                                        <div class="cart_count" style="color:white">0</div>

                                </div>
                                <div class="cart_content">

                                        <div class="cart_price">Rp. 0</div>

                                </div>
                            </div>
                        </div>
                    </a>
                    </div>
                </div>
            </div>

            <!-- Search -->
            <div class="col-lg-6 col-12 order-lg-2 order-3 text-lg-left text-right">
                <div class="header_search">
                    <div class="header_search_content">
                        <div class="header_search_form_container">
                            <form method="get" action="{{ url('frontend/produk-search') }}" class="header_search_form clearfix">
                                @csrf
                                <input name="search" type="search" required="required" class="header_search_input" placeholder="Search for products..." value="{{ isset($_GET['search']) ? $_GET['search'] : '' }}">
                                <div class="custom_dropdown" style="width:20px">
                                    <div class="custom_dropdown_list">
                                        <span class="custom_dropdown_placeholder clc">All Categories</span>
                                        <i class="fas fa-chevron-down"></i>
                                        <ul class="custom_list clc">
                                            <li class="kategori" data-nilai="">

                                                @foreach($kategoris as $kategori)
                                            <li class="kategori" data-nilai="{{$kategori['id']}}">

                                                <a class="clc" href="{{ url('frontend/produk-list/'.$kategori['slug'].'')}} ">{{$kategori['nama']}}</a>
                                            </li>
                                            @endforeach
                                            {{-- <li><a class="clc" href="#">Computers</a></li>
                                            <li><a class="clc" href="#">Laptops</a></li>
                                            <li><a class="clc" href="#">Cameras</a></li>
                                            <li><a class="clc" href="#">Hardware</a></li>
                                            <li><a class="clc" href="#">Smartphones</a></li> --}}
                                        </ul>
                                    </div>
                                </div>
                                {{-- {{ dd($kategoris) }} --}}
                                <button name="submit" id="submit" type="submit" class="header_search_button trans_300" value="Submit"><img src="{{ asset('images/search.png') }}" alt=""></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
