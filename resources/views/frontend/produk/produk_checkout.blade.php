@extends('frontend.layouts.utama')


@section('content')
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>

<script type="text/javascript">

</script>

<div class="row" style="margin-top:50px;">
    <div class="container">
    <form action="{{ route('order_simpan') }}" method="POST" accept-charset="utf-8">
        @csrf
        <div class="row">
        <div class="col-md-4 order-md-2 mb-4">


          <h4 class="d-flex justify-content-between align-items-center mb-3">
            <span class="text-muted"><i class="fa fa-shopping-basket"></i> Keranjang Anda</span>
          <span class="badge badge-danger badge-pill">{{ $total_quantity }}</span>
          <input type="hidden" name="total_produk" value="{{$total_quantity}}">
          </h4>
          <ul class="list-group mb-3">
            @foreach($items as $item)
                <li class="list-group-item d-flex justify-content-between lh-condensed">
                    <div>
                    <h6 class="my-0">{{ $item->name}}</h6>

                        <small class="text-muted">{{ $item->quantity}} x {{ format_rupiah($item->price)}}</small>
                    </div>
                <span class="text-muted">{{ format_uang($item->quantity * $item->price) }}</span>
                </li>
            @endforeach
            <li class="list-group-item d-flex justify-content-between">
              <span>Total (Rp)</span>
                <strong>
                    <span id="total" name="total" value="{{$total}}">{{format_rupiah($total)}}</span>
                    <input type="hidden" name="total" value="{{$total}}">
                </strong>
            </li>
          </ul>


        </div>
        <div class="col-md-8 order-md-1">
          <h4 class="mb-3">Alamat Pengiriman</h4>

            <div class="row">
              <div class="col-md-6 mb-3">
                <label for="nama_depan">Nama Depan</label>
                <input type="text" class="form-control" id="nama_depan" name="nama_depan" placeholder="" value="" required="">
                <div class="invalid-feedback">
                    Please enter a valid email address for shipping updates.
                </div>
              </div>
              <div class="col-md-6 mb-3">
                <label for="lastName">Nama Belakang</label>
                <input type="text" class="form-control" id="nama_belakang" name="nama_belakang" placeholder="" value="" required="">
                <div class="invalid-feedback">
                    Please enter a valid email address for shipping updates.
                </div>

              </div>
            </div>

            {{-- <div class="mb-3">
              <label for="username">Username</label>
              <div class="input-group">

                <input type="text" class="form-control" id="username" placeholder="Username" required="">
                <div class="invalid-feedback" style="width: 100%;">
                  Your username is required.
                </div>
              </div>
            </div> --}}

            <div class="mb-3">
              <label for="email">Email </label>
              <input type="email" class="form-control" id="email" name="email" placeholder="you@example.com">
              <div class="invalid-feedback">
                Please enter a valid email address for shipping updates.
              </div>
            </div>

            <div class="mb-3">
              <label for="address">Alamat</label>
              <textarea class="form-control" name="alamat_lengkap" id="alamat_lengkap" placeholder="Alamat Lengkap" rows="5" cols="5" required></textarea>
              <div class="invalid-feedback">
                Please enter your shipping address.
              </div>
            </div>

            <div class="row">

              <div class="col-md-4 mb-3">
                <label for="state">Kota</label>
                 <input type="text" class="form-control" id="kota" name="kota" placeholder="" required="">
                <div class="invalid-feedback">
                  Please provide a valid state.
                </div>
              </div>
              <div class="col-md-3 mb-3">
                <label for="zip">Kode Pos</label>
                <input type="text" class="form-control" id="kode_pos" name="kode_pos" name placeholder="" required="">
                <div class="invalid-feedback">
                  Zip code required.
                </div>
              </div>
            </div>
            <div class="mb-3">
              <label for="no_handphone">No Handphone </label>
              <input type="number" class="form-control" id="no_handphone" name="no_handphone" placeholder="08xxxx">
              <div class="invalid-feedback">
                Please enter a valid email address for shipping updates.
              </div>
            </div>
            <hr class="mb-4">
              <hr class="mb-4">
              <h4 class="mb-3">Kurir</h4>
            <div class="d-block my-3">
              <!-- Default unchecked -->
                <div class="input-group">
                    @foreach($kurirs as $kurir)
                    <div class="form-control">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                            <input type="radio" name="kurir_id" id="kurir_id" value="{{ $kurir->id }}" aria-label="Radio button for following text input" required>
                            </div>

                        </div>
                    &nbsp;<label>{{ $kurir->nama }}</label>&nbsp;|&nbsp;<label>{{ format_rupiah($kurir->harga) }}</label>
                    </div>
                    @endforeach
                </div>
            </div>
            <hr class="mb-4">
              <hr class="mb-4">
              <h4 class="mb-3">Metode Pembayaran</h4>
            <div class="d-block my-3">
              <!-- Default unchecked -->
                <div class="input-group">
                    <div class="form-control">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                            <input type="radio" name="payment_method" value="BCA" required>
                            </div>
                        </div>
                        &nbsp;<label>Transfer Bank</label>&nbsp;|&nbsp;<label>BCA No Rek:023750237</label>
                    </div>
                </div>
            </div>

            <hr class="mb-4">
            <center>
                <button type="submit" class="button cart_button" id="submit">Process Checkout</button>
            </center>

        </div>
      </div>
     </form>
    </div>
</div>
<!-- akhir baris kedua -->
@endsection

