@extends('frontend.layouts.utama')


@section('content')

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
<script type="text/javascript">

     $(document).ready(function () {

        // load_cart_items();
        details();


        $('.remove_item').click(function() {
            var id = $(this).attr('value-id');
            remove_item(id);
        });

        $('.quantity_input').keyup( function (e) {
            var total_amount = 0;
            var total_quantity = 0;
            $('.cart_item_quantity').each(function (e) {

                var i = $('.quantity_input', this);

                var harga = parseFloat($(i).data('unit-price'));
                var quantity = parseFloat($(i).val());

                total_quantity = total_quantity + quantity ;
                total_amount = total_amount + (harga * quantity);


            });

            // Subtotal price
            $('#quantity_total').html(total_quantity);
            $('#total_amount').html('Rp. '+formatNumber(total_amount));
        });
        $('.quantity_input').keydown( function (e) {
            limitCharacter(event);
        });

    });



    function clear_cart () {
        $.ajax ({
            headers: {
               'X-CSRF-TOKEN': '{!! csrf_token() !!}'
            },
            url:"{{ route('clear_cart') }}",
            method:'GET',
            data:{
                    },
            dataType:'json',

            success:function(data)
            {
                swal("Success!", "Clear Cart Berhasil !", "success");
            }
        })

    }

    function details() {

        $.ajax({
            headers: {
               'X-CSRF-TOKEN': '{!! csrf_token() !!}'
            },
            url:"{{ route('details_cart') }}",
            method:'GET',
            data:{
                    },
            dataType:'json',

            success:function(data)
            {
                $('#quantity_total').html(data.data.total_quantity);
                $('.order_total_amount').html('Rp. '+formatNumber(data.data.total));

            }
        })
    }

    function remove_item(id) {
        $.ajax ({
            headers: {
               'X-CSRF-TOKEN': '{!! csrf_token() !!}'
            },
            url:" /frontend/delete_item/"+id+"",
            method:'GET',
            data:{
                    },
            dataType:'json',

            success:function(data)
            {

                swal("Success!", data.message,"success")
                .then((value) => {
                    location.reload();
                });
                // swal("Success!", data.message,"success");
            }
        })
    }

    function formatNumber(num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }

</script>


<div class="cart_section">
		<div class="container" style="max-width: 1300px;">
			<div class="row">
				<div class="col-lg-10 offset-lg-1">
					<div class="cart_container">
						<div class="cart_title">Shopping Cart</div>
						<div class="cart_items">
                            <form action="{{route('update_quantity_cart')}}" method="POST">
                             @csrf
                            <ul class="cart_list">
                            @if(count($items) > 0)
                            @foreach($items as $key => $val)
                                <li class="cart_item clearfix">
                                    <div class="cart_item_image"><img src="{{ asset('uploads/products/'.$val['attributes']['gambar'].'') }}" alt=""></div>
                                    <div class="cart_item_info d-flex flex-md-row flex-column justify-content-between">

                                        <div class="cart_item_name cart_info_col"  style="width:200px"  >
                                            <div class="cart_item_title">Name</div>
                                            <div class="cart_item_text">{{ $val['name'] }}</div>
                                        </div>

                                        <div class="cart_item_quantity cart_info_col">
                                            <div class="cart_item_title">Quantity</div>
                                            <!-- Product Quantity -->
                                            <br>

                                            <input type="text" name="quantity_input_{{$key}}" data-unit-price="{{$val['price']}}" class="form-control quantity_input" type="text" pattern="[0-9]*" value="{{ $val['quantity'] }}" data-rule="quantity" style="width:70px;height:50px" required>

                                        </div>


                                        <div class="cart_item_price cart_info_col">
                                            <div class="cart_item_title">Price</div>
                                            <div class="cart_item_text price" id="price_{{$key}}">{{ format_rupiah($val['price']) }}</div>
                                        </div>
                                        {{-- <div class="cart_item_total cart_info_col">
                                            <div class="cart_item_title">Total</div>
                                            <div class="cart_item_text total">{{ $val['price'] * $val[ 'quantity']}}</div>
                                        </div> --}}
                                        <div class="cart_item_name cart_info_col">
                                            <div class="cart_item_title">Delete</div>
                                            <div class="cart_item_text"><button type="button" class="btn btn-primary remove_item" value-id="{{ $val['id'] }}"><i class="fa fa-trash"></i></button>

                                            </div>
                                        </div>
                                </li>
                            @endforeach
                            @endif
                            </ul>

                        </div>
                    </div>


                    <!-- Order Total -->
                    <div class="order_total">
                        <div class="order_total_content text-md-right">
                        <div class="order_total_title">Quantity Total : <span class="cart_item_text" style="color:black;" id="quantity_total">&nbsp; {{ format_uang($total_quantity) }}</span></div>
                            <div class="order_total_title">&nbsp; Order Total :</div>
                        <div class="order_total_amount" id="total_amount"></div>
                        </div>
                    </div>


						<div class="cart_buttons">
                        {{-- <a class="button cart_button_clear" href="{{ url('frontend/produk-list') }}">Continue Shopping</a> --}}
                            <a class="btn btn-lg btn-warning float-left" href="{{ route('frontend') }}">Continue Shopping</a>
                            <a class="button cart_button_clear" href="{{ route('clear_cart') }}">Clear Cart</a>

                            <button type="submit" class="button cart_button_checkout" >Go to Checkout</button>


                        </form>


                            {{-- <div class="product_quantity clearfix" data-trigger="spinner">
                                <span>Quantity: </span>
                                <input id="quantity_input2" type="text" pattern="[0-9]*" value="1" data-rule="quantity" >
                                <div class="quantity_buttons">
                                    <div id="quantity_inc_button" class="spin-up quantity_inc quantity_control" data-spin="up">
                                        <i class="fas fa-chevron-up"></i></div>
                                    <div id="quantity_dec_button" class="spin-down quantity_dec quantity_control" data-spin="down">
                                        <i class="fas fa-chevron-down"></i></div>
                                </div>
                            </div> --}}

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- akhir baris kedua -->

@endsection

