<?php

namespace App\Listeners;

use App\Events\KaryawanWasCreated;
use App\Mail\KaryawanCreated as KaryawanCreatedMail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendKaryawanCreatedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  KaryawanCreated  $event
     * @return void
     */
    public function handle(KaryawanWasCreated $event)
    {
        Mail::to($event->karyawan->email)->send(
            new KaryawanCreatedMail($event->karyawan)
        );
    }
}
