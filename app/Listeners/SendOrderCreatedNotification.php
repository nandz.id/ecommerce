<?php

namespace App\Listeners;

use App\Events\OrderWasCreated;
use App\Mail\OrderCreated as OrderCreatedMail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendOrderCreatedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderCreated  $event
     * @return void
     */
    public function handle(OrderWasCreated $event)
    {
        // dd($event->order_produks);
        Mail::to($event->order->email)->send(
            new OrderCreatedMail($event->order,$event->order_produks, $event->pdf)
        );
    }
}
