<?php



use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Darryldecode\Cart\Cart;
use Darryldecode\Cart\CartCondition;

if (!function_exists('upload_image')) {
    function upload_image($path,$dimensions,$request) {

        if (!File::isDirectory($path)) {
            //MAKA FOLDER TERSEBUT AKAN DIBUAT
            File::makeDirectory($path);
        }

        //MENGAMBIL FILE IMAGE DARI FORM
        $file = $request;
        //MEMBUAT NAME FILE DARI GABUNGAN TIMESTAMP DAN UNIQID()
        $fileName = Carbon::now()->timestamp . '_' . uniqid() . '.' . $file->getClientOriginalExtension();
        //UPLOAD ORIGINAN FILE (BELUM DIUBAH DIMENSINYA)
        Image::make($file)->save($path . '/' . $fileName);

        //LOOPING ARRAY DIMENSI YANG DI-INGINKAN
        //YANG TELAH DIDEFINISIKAN PADA CONSTRUCTOR
        foreach ($dimensions as $row) {
            //MEMBUAT CANVAS IMAGE SEBESAR DIMENSI YANG ADA DI DALAM ARRAY
            $canvas = Image::canvas($row, $row);
            //RESIZE IMAGE SESUAI DIMENSI YANG ADA DIDALAM ARRAY
            //DENGAN MEMPERTAHANKAN RATIO
            $resizeImage  = Image::make($file)->resize($row, $row, function($constraint) {
                $constraint->aspectRatio();
            });

            //CEK JIKA FOLDERNYA BELUM ADA
            if (!File::isDirectory($path . '/' . $row)) {
                //MAKA BUAT FOLDER DENGAN NAMA DIMENSI
                File::makeDirectory($path . '/' . $row);
            }

            //MEMASUKAN IMAGE YANG TELAH DIRESIZE KE DALAM CANVAS
            $canvas->insert($resizeImage, 'center');
            //SIMPAN IMAGE KE DALAM MASING-MASING FOLDER (DIMENSI)
            $canvas->save($path . '/' . $row . '/' . $fileName);
        }
        return $fileName;
    }
}


if (! function_exists('autonumber')) {
    function autonumber($table='', $primary='', $prefix='', $length =0){
        $create_nik=DB::table($table)->select(DB::raw('MAX(RIGHT('.$primary.','.$length.')) as auto_number'))->get();

        if($create_nik->count()>0)
        {
            foreach($create_nik as $k)
            {
                $tmp = ((int)$k->auto_number)+1;
                $kode = $prefix.sprintf("%0".$length."s", $tmp);
            }
        }
        else
        {
            $nol = [];
            for($i=1; $i <= $length; $i++) {
                $nol[] = '0';
            }
            $nol = implode(',', $nol);
            $kode = $prefix."".$nol."1";
        }

        return $kode;
    }
}



if (! function_exists('tgl_id')) {
    function tgl_id($tgl)
    {
        $dt = new Carbon($tgl);
        setlocale(LC_TIME, 'id');
        return $dt->formatLocalized('%d %B %Y');
    }
}

if (!function_exists('rupiah'))
{
	function rupiah($nilai)
	{
		return number_format($nilai,0,',',',');
	}
}

if (!function_exists('format_rupiah'))
{
	function format_rupiah($nilai)
	{
		return 'Rp.'.number_format($nilai,0,',','.');
	}
}

if (!function_exists('format_uang'))
{
	function format_uang($nilai)
	{
		return number_format($nilai,0,'.',',');
	}
}

if (!function_exists('format_angka'))
{
	function format_angka($nilai)
	{
		return number_format($nilai,2,'.',',');
	}
}

if (!function_exists('limit_text'))
{
    function limit_text($text)
    {
        $string = strip_tags($text);
        if (strlen($string) > 20) {

            // truncate string
            $stringCut = substr($string, 0, 20);
            $endPoint = strrpos($stringCut, ' ');

            //if the string doesn't contain any space then it will cut without word basis.
            $string = $endPoint ? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
            $string .= '...';
        }

        return $string;
    }
}

if(!function_exists('content_cart'))
{
    function content_cart($userId)
    {
        $items = array();

        \Cart::session($userId)->getContent()->each(function ($item) use (&$items) {

            $items[] = $item;
        });

        $produks = [];
        foreach ($items as $key => $val) {
            $produks[] = App\Models\Produk::where('id', $val['id'])->get()->toArray();

            $items[$key]['produk_sku'] = $produks[$key][0]['sku'];
            $items[$key]['produk_nama'] = $produks[$key][0]['nama'];
            $items[$key]['produk_deskripsi'] = $produks[$key][0]['deskripsi'];
        }
        // dd($items);

        return $items;
    }
}
