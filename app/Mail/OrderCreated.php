<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderCreated extends Mailable
{
    use Queueable, SerializesModels;

    public $order;
    public $order_produks;
    public $alamat;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($order,$order_produks,$pdf)
    {
        $this->order = $order;
        $this->order_produks = $order_produks;
        $this->pdf = $pdf;
        // dd($this->order_produks);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->from('nandzo@coba.com')
                    ->subject('Order Produk Pembelian dengan kode Order '.$this->order->order_kode.' telah berhasil')
                    ->markdown('emails.order-created')
                    ->attachData($this->pdf->output(), "invoice_".$this->order->invoice_no.".pdf");;
    }
}
