<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Kategori;
use Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        view()->composer('frontend.layouts.header', function ($view) {
            $view->with('kategoris', $this->getKategoris());
        });

        view()->composer('frontend.layouts.menu', function ($view) {
            $view->with('kategoris', $this->getKategoris());
        });


    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function getKategoris() {
        $kategoris = Kategori::all();
        return $kategoris;
    }
}
