<?php

namespace App\Http\Controllers;
// namespace App\Models;

use DB;
use Charts;
use Notification;
use App\Models\Order;
use App\Models\Alamat;
use App\Models\Produk;
use App\Models\Kategori;
use App\Models\OrderProduk;
use Illuminate\Http\Request;
use App\Notifications\MyFirstNotification;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $where = [
            ['status_bayar', '=', 'accepted'],
            ['konfirmasi_bayar', '=', 'confirmed']
        ];
        $count_kategori = Kategori::count();
        $count_produk   = Produk::count();
        $count_customer = Alamat::count();
        $count_order    = Order::count();


        $count_produk_terjual = DB::table('order_produk as op')
                        ->leftJoin('orders as o', 'op.id', '=', 'o.id')
                        ->select('op.id')
                        ->orderBy('k.id', 'asc')
                        ->where([['o.status_bayar', '=', 'accepted'],['o.konfirmasi_bayar', '=', 'confirmed']])
                        ->count();
        $count_order_sukses = Order::where($where)->count();
        $count_total_penjualan = Order::where($where)->sum('total');

        $orders      = Order::where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"), date('Y'))->get();
        $chart_order = Charts::database($orders, 'line', 'highcharts')
                        ->title("Monthly New Orders")
                        ->elementLabel("Total Orders")
                        ->dimensions(500, 250)
                        ->responsive(false)
                        ->groupByMonth(date('Y'), true);
        // dd($count_total_penjualan);
        // SELECT kategori.nama,COUNT(*)
        // FROM produk join kategori on kategori.id = produk.kategori_id
        // GROUP BY kategori_id;

        $produk_per_kategori = DB::table('produk as p')
            ->leftJoin('kategori as k', 'k.id', '=', 'p.kategori_id')
            ->select('k.nama as nama',DB::raw('COUNT(*) as total_produk'))
            ->groupBy('kategori_id')
            ->get();
        // dd($produk_per_kategori);
        $data = [];

        foreach($produk_per_kategori as $val) {
            $data['nama'][] = $val->nama;
            $data['total_produk'][] = $val->total_produk;
        }
        // dd($data['nama']);

        $kategori_produk  = Charts::database($orders, 'pie', 'highcharts')
                            ->title('Produk per Category')
                            ->labels($data['nama'])
                            ->values($data['total_produk'])
                            ->dimensions(460, 250)
                            ->responsive(false);

        return view('home', compact('count_produk', 'count_customer', 'count_order', 'count_produk_terjual','count_kategori', 'count_order_sukses','count_total_penjualan','chart_order', 'kategori_produk'));
    }

    public function sendNotification() {
        $user = Karyawan::first();
        $details = [
            'greeting'   => 'hello World',
            'body'       => 'This is my notification from nandz.id@gmail.com',
            'thanks'     => 'Thanks you bla bla bla',
            'actionText' => 'View My Site',
            'actionURL'  => url('/'),
            'order_id'   => 101
        ];

        Notification::send($user, new MyFirstNotification($details));
        dd('done');
        // Notification::route('mail', Auth::user()->email)->notify;
    }
}
