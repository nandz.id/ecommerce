<?php

namespace App\Http\Controllers;

use Alert;
use Validator;
use App\Models\Atribut;
use App\Models\AtributValue;
use Illuminate\Http\Request;
use Form;



class AtributController extends Controller
{
    public function __construct() {

        $this->class_error = "has-error";
    }

    public function index() {

        $atribut = Atribut::all();

        return view('atribut.index', compact('atribut'));

    }

    public function atributList()
    {
        $atribut = Atribut::all();

        return datatables()->of($atribut)
            ->addIndexColumn()
            ->addColumn('action', function ($atribut) {
                return '<div class="row">
                            <div class="col-md-6 text-right" style="padding-left:0px;padding-right:0px">
                                <form class="delete_form" action=' . action('AtributController@destroy', $atribut->id).'" method="post">
                                    <a href="'.action( 'AtributController@edit', $atribut->id).'" class="btn btn-sm btn-success test123" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
                                    ' . Form::token() . '
                                    <input name="_method" type="hidden" value="DELETE">
                                    <input type="submit" name="submit" class="submit" id="hapus_'.$atribut->id.'" style="display:none;">
                                </form>
                            </div>
                            <div class="col-md-6 text-left" style="padding-left:0px;padding-right:0px">
                                <button class="btn btn-sm btn-danger klik" data-toggle="tooltip" value-id ="'.$atribut->id.'" title="Edit"> <i class="fa fa-trash"></i></button>
                            </div>
                        </div>';
            })
            ->editColumn('id', 'ID: {{$id}}')
            ->make(true);

    }

    public function create() {

        $class_error = $this->class_error;
        return view('atribut.create', compact('class_error'));
    }

    public function store (Request $request) {

        $this->validate($request, [
            'tipe' => 'required|string|max: 50',
            'nama'  => 'required|string|max: 255',
        ],
        [
            'nama.required' => 'Nama Atribut wajib diisi !',
            'tipe.required' => 'Tipe wajib dipilih !',

        ]);

        $atribut = new Atribut;
        $atribut->tipe = $request->tipe;
        $atribut->nama = $request->nama;
        $atribut->save();

        $rules_option = [];
        foreach($request->input('option') as $key => $value) {
            $rules_option["option.{$key}"] = 'required';
        }

        $validator = Validator::make($request->all(), $rules_option);

        if ($validator->passes()) {
            foreach($request->input('option') as $key => $value) {
                AtributValue::create([
                    'atribut_id' => $atribut->id,
                    'value' => $value
                ]);
            }
            return response()->json(['success'=>'done']);
        }

        return response()->json(['error'=>$validator->errors()->all()]);
    }

    public function show($id) {
        $atribut = Atribut::find($id);
        if ($atribut) {
          return response()->json(['status' => 'success', 'data'=> $atribut]);
        }

        return response()->json(['status' => 'error', 'message' => 'Data not found'],404);
    }

    public function edit($id) {

        $atribut = Atribut::find($id);
        $class_error = $this->class_error;
        return view('atribut.edit', compact('atribut','class_error', 'id'));

    }

    public function update (Request $request, $id) {

        $this->validate($request, [
            'nama_atribut' => 'required|string|max: 50',
            'keterangan'  => 'required|string|max: 255'
        ],
        [
            'nama_atribut.required' => 'Nama Atribut wajib diisi !',
            'keterangan.required'  => 'Keterangan wajib diisi !'
        ]);

        $atribut = Atribut::find($id);
        $atribut->nama_atribut = $request->nama_atribut;
        $atribut->keterangan  = $request->keterangan;
        $atribut->save();
        Alert::success('Data Atribut berhasil diubah !','Success');


        return redirect('atribut')->with('Success', 'Data Atribut berhasil diubah !');


    }

    public function destroy ($id) {
        $atribut = Atribut::find($id);
        $atribut->delete();
        Alert::success('Data Atribut berhasil dihapus !','Success');

        return redirect('atribut')->with('Success', 'Data berhasil di delete');

    }

    public function laporanExcel()
    {
        return Excel::download(new AtributReport, 'atribut.xlsx');
    }





}
