<?php

namespace App\Http\Controllers;
// namespace App\Models;

use Alert;

use Datatables;
use Carbon\Carbon;
use App\Models\Divisi;
use App\Models\Jabatan;
use App\Models\Karyawan;
use DB, Response, Config;
use Illuminate\Http\Request;
use App\Events\KaryawanWasCreated;
use Illuminate\Support\Facades\Mail;

class KaryawanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct () {
        $this->class_error ="has-error";
        $this->path = storage_path('app/public/img');
        //DEFINISIKAN DIMENSI
        $this->dimensions = ['245', '300', '500'];
    }

    public function index()
    {
        $karyawan = DB::table('karyawan as k')
                    ->leftJoin('jabatan as j', 'k.jabatan_id', '=', 'j.id')
                    ->select('k.*','j.nama_jabatan')
                    ->orderBy('k.id', 'asc')
                    ->get();
        // $karyawan = Karyawan::leftJoin('jabatan');
        // $karyawan = Karyawan::all();
        // dd($karyawan);
        // die();
        return view ('karyawan.index',compact('karyawan'));
    }

    // public function userList()
    // {
    //     $karyawan = DB::table('karyawan as k')
    //                 ->leftJoin('jabatan as j', 'k.jabatan_id', '=', 'j.id')
    //                 ->select('k.*','j.nama_jabatan')
    //                 ->orderBy('k.id', 'asc')
    //                 ->get();
    //     return datatables()->of($karyawan)
    //             ->make(true);
    // }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $nik     = autonumber(Karyawan::getTableName(),'nik','PGT',4);
        $jabatan = Jabatan ::all();
        $class_error = $this->class_error;
        return view('karyawan.create',compact('jabatan','class_error','nik'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(Request $request)
    {
        $this->validate($request, [
            'nik'           => 'required|string|max:10',
            'nama_lengkap'  => 'required|string|max: 255',
            'tempat_lahir'  => 'required|string|max: 255',
            'tanggal_lahir' => 'required|date_format:"Y/m/d"',
            'jk'            => 'required|string|max: 1',
            'jabatan_id'    => 'required|int|max: 11',
            'email'         => 'required|email',
            'alamat'        => 'required|string|max: 255',
            'foto'          => 'required|image|mimes: jpg,jpeg,bmp,png|max:2000'
        ],
        [
            'nik.required'           => 'NIK harus diisi !',
            'nama_lengkap.required'  => 'Masukkan nama lengkap anda, wajib diisi !',
            'tempat_lahir.required'  => 'Masukan tempat lahir, wajib diisi',
            'tanggal_lahir.required' => 'Masukan tanggal lahir, wajib diisi',
            'jk.required'            => 'Pilih Jenis Kelamin !',
            'jabatan_id.required'    => 'Pilih Jabatan !',
            'email.reaquired'        => 'Masukan alamat email dengan benar',
            'alamat.required'        => 'Masukkan alamat lengkap, wajib diisi !',
            'foto.required'          => 'Masukan foto, wajib diisi !'
        ]
        );

        //JIKA FOLDERNYA BELUM ADA

        $fileName = upload_image($this->path, $this->dimensions, $request->file('foto'));

        // $karyawan = new Karyawan;

        // $karyawan->nik           = Karyawan::autonumber();
        // $karyawan->nama_lengkap  = $request->get('nama_lengkap');
        // $karyawan->tempat_lahir  = $request->get('tempat_lahir');
        // $karyawan->tanggal_lahir = $request->get('tanggal_lahir');
        // $karyawan->jk            = $request->get('jk');
        // $karyawan->jabatan_id    = $request->get('jabatan_id');
        // $karyawan->alamat        = $request->get('alamat');
        // $karyawan->foto          = $fileName;
        // $karyawan->save();

        $data = request()->all();
        $data['foto'] = $fileName;
        $karyawan = Karyawan::create($data);

        event(new KaryawanWasCreated($karyawan));


        Alert::success('Data Karyawan berhasil ditambah !','Success');

        return redirect('karyawan')->with('success', 'Data karyawan telah ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $karyawan = Karyawan::find($id);
        $jabatan  = Jabatan::all();
        $class_error = $this->class_error;
        return view('karyawan.show',compact('karyawan','jabatan','class_error', 'id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $karyawan = Karyawan::find($id);
        $jabatan   = Jabatan::all();
        $class_error = $this->class_error;
        return view('karyawan.edit',compact('karyawan','jabatan','class_error', 'id'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama_lengkap'  => 'required|string|max: 255',
            'tempat_lahir'  => 'required|string|max: 255',
            'tanggal_lahir' => 'required|date',
            'jk'            => 'required|string|max: 1',
            'jabatan_id'    => 'required|int|max: 11',
            'email'         => 'required|email',
            'alamat'        => 'required|string|max: 255',
            'foto'          => 'image|mimes: jpg,jpeg,bmp,png|max:2000'
        ],
        [
            'nama_lengkap.required'  => 'Masukkan nama lengkap anda, wajib diisi !',
            'tempat_lahir.required'  => 'Masukan tempat lahir, wajib diisi',
            'tanggal_lahir.required' => 'Masukan tanggal lahir, wajib diisi',
            'jk.required'            => 'Pilih Jenis Kelamin !',
            'jabatan_id.required'    => 'Pilih Jabatan !',
            'email.required'         => 'Masukan alamat email dengan benar',
            'alamat.required'        => 'Masukkan alamat lengkap, wajib diisi !',
            // 'foto.required'          => 'Masukan foto, wajib diisi !'
        ]
        );

        $karyawan = Karyawan::find($id);
        $karyawan->nama_lengkap  = $request->get('nama_lengkap');
        $karyawan->tempat_lahir  = $request->get('tempat_lahir');
        $karyawan->tanggal_lahir = $request->get('tanggal_lahir');
        $karyawan->jk            = $request->get('jk');
        $karyawan->jabatan_id    = $request->get('jabatan_id');
        $karyawan->email         = $request->get('email');
        $karyawan->alamat        = $request->get('alamat');
        if($request->file('foto') != "") {
            $fileName = upload_image($this->path, $this->dimensions, $request->file('foto'));
            $karyawan->foto = $fileName;
        }
        // print_r($karyawan);
        // die();
        $karyawan->save();
        Alert::success('Data Karyawan berhasil diubah !','Success');

        return redirect('karyawan')->with('success', 'Data Karyawan sukses di update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $karyawan = Karyawan::find($id);
        $karyawan->delete();
        Alert::error('Data Karyawan berhasil dihapus !','Success');

        return redirect('karyawan')->with('success','Data Karyawan telah di hapus');
    }
}
