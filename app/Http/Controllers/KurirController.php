<?php

namespace App\Http\Controllers;

use Alert;
use Illuminate\Http\Request;
use App\Models\Kurir;
use Datatables;
use DB;
use Form;
use App\DataTables\KurirDataTable;




class KurirController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
        $this->class_error = "has-error";
        $this->path = storage_path('app/public/img');
        //DEFINISIKAN DIMENSI
        $this->dimensions = ['245', '300', '500'];
    }

    public function index()
    {

        $kurir = Kurir::all();
        return view('kurir.index', compact('kurir'));
    }

    public function kurir_list()
    {
        $kurir = Kurir::all();

        return datatables()->of($kurir)
            ->addIndexColumn()
            ->addColumn('action', function ( $kurir) {
                return '<div class="row">
                            <div class="col-md-6 text-right" style="padding-left:0px;padding-right:0px">
                                <form class="delete_form" action=' . action('KurirController@destroy', $kurir->id) . '" method="post">
                                    <a href="' . action('KurirController@edit', $kurir->id) . '" class="btn btn-sm btn-success test123" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
                                    ' . Form::token() . '
                                    <input name="_method" type="hidden" value="DELETE">
                                    <input type="submit" name="submit" class="submit" id="hapus_' . $kurir->id . '" style="display:none;">
                                </form>
                            </div>
                            <div class="col-md-6 text-left" style="padding-left:0px;padding-right:0px">
                                <button class="btn btn-sm btn-danger klik" data-toggle="tooltip" value-id ="' . $kurir->id . '" title="Edit"> <i class="fa fa-trash"></i></button>
                            </div>
                        </div>';
            })
            ->editColumn('id', 'ID: {{$id}}')
            ->make(true);
    }

    public function create()
    {
        $class_error = $this->class_error;
        return view('kurir.create', compact('class_error'));
    }

    public function store(Request $request)
    {

        $this->validate(
            $request,
            [
                'nama'      => 'required|string|max : 100',
                'harga'     => 'required|numeric',
                'deskripsi' => 'required|string|max : 255',
                'logo'      => 'required|image|mimes: jpg,jpeg,bmp,png|max: 2000'
            ],
            [
                'nama.required'      => 'Nama Kurir wajib diisi !',
                'harga.required'     => 'Harga wajib diisi !',
                'deskripsi.required' => 'Notifikasi wajib dipilih',
                'logo.required'      => 'Masukan foto, wajib diisi !'
            ]
        );

        $kurir = new Kurir;
        $kurir->nama      = $request->nama;
        $kurir->harga     = $request->harga;
        $kurir->deskripsi = $request->deskripsi;

        $fileName = upload_image($this->path, $this->dimensions, $request->file('logo'));


        $data = request()->all();
        $data['logo'] = $fileName;
        $kurir = Kurir::create($data);

        Alert::success('Data Kurir berhasil ditambahkan !', 'Success');
        return redirect('kurir')->with('Success', 'Data Kurir berhasil ditambahkan !');
    }

    public function edit($id)
    {

        $kurir = Kurir::find($id);
        $class_error = $this->class_error;
        return view('kurir.edit', compact('kurir', 'class_error', 'id'));
    }

    public function update(Request $request, $id)
    {

        $this->validate(
            $request,[
                'nama'      => 'required|string|max: 100',
                'harga'     => 'required|numeric',
                'deskripsi' => 'required|string|max: 255',
                'logo'      => 'image|mimes: jpg,jpeg,bmp,png|max: 2000'
            ],
            [
                'nama.required'      => 'Nama Kurir wajib diisi !',
                'harga.required'     => 'Harga wajib diisi !',
                'deskripsi.required' => 'Notifikasi wajib dipilih',
                'logo.required'      => 'Masukan logo, wajib diisi !'
            ]
        );

        $kurir = Kurir::find($id);
        $kurir->nama      = $request->nama;
        $kurir->harga     = $request->harga;
        $kurir->deskripsi = $request->deskripsi;

        if ($request->file('logo') != "") {
            $fileName = upload_image($this->path, $this->dimensions, $request->file('logo'));
            $kurir->logo = $fileName;
        }

        $kurir->save();

        Alert::success( 'Data Kurir berhasil diubah !', 'Success');

        return redirect('kurir')->with('success', 'Data Kurir berhasil diubah !');
    }

    public function destroy($id)
    {
        $kurir = Kurir::find($id);
        $kurir->delete();
        Alert::success( 'Data Kurir berhasil hapus !', 'Success');
        return redirect('kurir')->with('Success', 'Data berhasil di delete');
    }

    public function laporanExcel()
    {
        return Excel::download(new KurirReport, 'Kurir.xlsx');
    }
}
