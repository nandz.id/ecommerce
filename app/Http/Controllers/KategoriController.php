<?php

namespace App\Http\Controllers;

use Alert;
use Illuminate\Http\Request;
use App\Models\Kategori;
use Datatables;
use DB;
use Form;
use App\DataTables\KategoriDataTable;




class KategoriController extends Controller
{
    public function __construct() {
        // $this->middleware('auth');
        $this->class_error = 'has-error';
    }

    public function index() {

        $kategori = Kategori::all();
        return view('kategori.index', compact('kategori'));

    }

    public function kategoriList()
    {
        $kategori = Kategori::all();

        return datatables()->of($kategori)
            ->addIndexColumn()
            ->addColumn('action', function ($kategori) {
                return '<div class="row">
                            <div class="col-md-6 text-right" style="padding-left:0px;padding-right:0px">
                                <form class="delete_form" action='.action('KategoriController@destroy', $kategori->id).'" method="post">
                                    <a href="'.action('KategoriController@edit', $kategori->id).'" class="btn btn-sm btn-success test123" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
                                    '.Form::token(). '
                                    <input name="_method" type="hidden" value="DELETE">
                                    <input type="submit" name="submit" class="submit" id="hapus_'.$kategori->id.'" style="display:none;">
                                </form>
                            </div>
                            <div class="col-md-6 text-left" style="padding-left:0px;padding-right:0px">
                                <button class="btn btn-sm btn-danger klik" data-toggle="tooltip" value-id ="'. $kategori->id.'" title="Edit"> <i class="fa fa-trash"></i></button>
                            </div>
                        </div>';
            })
            ->editColumn('id', 'ID: {{$id}}')
            ->make(true);

    }

    public function create() {

        $class_error = $this->class_error;

        return view('kategori.create', compact('class_error'));
    }

    public function store (Request $request) {

        $this->validate($request, [
            'nama'       => 'required|string|max: 100',
            'slug'       => 'required|string|max: 50',
            
        ],
        [
            'nama.required' => 'Nama Kategori wajib diisi !',
            'slug.required' => 'Slug wajib diisi !',
            
        ]);

        $kategori = new Kategori;
        $kategori->nama       = $request->nama;
        $kategori->slug       = $request->slug;
        $kategori->keterangan = $request->keterangan;

        $kategori->save();
        Alert::success('Data kategori berhasil ditambahkan !','Success');
        return redirect('kategori')->with('Success','Data kategori berhasil ditambahkan !');

    }

    public function edit($id) {

        $kategori = Kategori::find($id);
        $class_error = $this->class_error;
        return view('kategori.edit', compact('kategori','class_error', 'id'));

    }

    public function update (Request $request, $id) {

        $this->validate($request, [
            'nama'       => 'required|string|max: 100',
            'slug'       => 'required|string|max: 50',
            
        ],
        [
            'nama.required' => 'Nama Kategori wajib diisi !',
            'slug.required' => 'Slug wajib diisi !',
            
        ]);

        $kategori = Kategori::find($id);
        $kategori->nama       = $request->nama;
        $kategori->slug       = $request->slug;
        $kategori->keterangan = $request->keterangan;
        $kategori->save();
        Alert::success('Data kategori berhasil diubah !','Success');

        return redirect('kategori')->with('Success', 'Data kategori berhasil diubah !');
    }

    public function destroy ($id) {
        $kategori = Kategori::find($id);
        $kategori->delete();
        Alert:: success('Data kategori berhasil hapus !','Success');
        return redirect('kategori')->with('Success', 'Data berhasil di delete');
    }

    public function laporanExcel()
    {
        return Excel::download(new KategoriReport, 'divisi.xlsx');
    }







}
