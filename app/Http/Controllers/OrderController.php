<?php

namespace App\Http\Controllers;
// namespace App\Models;
use PDF;
use Alert;
use Datatables;
use Carbon\Carbon;
use App\Models\Order;
use App\Models\Alamat;
use App\Models\Produk;
use App\Models\Jabatan;
use App\Models\Kategori;
use DB, Response, Config;
use App\Models\OrderProduk;
use App\Models\OrderStatus;
use App\Models\ProdukGambar;
use Illuminate\Http\Request;
use App\Events\OrderWasCreated;
use App\Models\OrderStatusHistory;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function __construct () {
        $this->class_error ="has-error";
        $this->path = storage_path('app/public/img');
        //DEFINISIKAN DIMENSI
        $this->dimensions = ['245', '300', '500'];


    }

    public function index()
    {
        $orders = Order::with(['alamat','order_status'])->get();

        return view ('order.index',compact('orders'));
    }

    public function view ($id) {

        $orders = Order::with(['alamat', 'order_status', 'kurir'])->where('orders.id','=', $id)->first();
        $order_produks = OrderProduk::where('order_id',$id)->get();
        $order_status_history = OrderStatusHistory::with('order_status')->where('order_id',$id)->get();
        // dd($order_status_history);
        $order_status = OrderStatus::all();
        // dd($orders);

        return view ('order.order_view',compact('id', 'orders', 'kurirs', 'order_status', 'order_produks', 'order_status_history'));

    }

    public function update_order(Request $request, $id) {

        $orders = Order::find($id);


        $orders->order_status_id = $request->get('status_id');
        // dd($orders);
        $orders->save();
        OrderStatusHistory::create([
            'order_id'        => $id,
            'order_status_id' => $request->get('status_id')
        ]);

        Alert::success('Status Order berhasil diubah !', 'Success');
        return redirect('orders/order_view/'.$id.'')->with('success', 'Status Order sukses di update');

    }

    public function update_pembayaran(Request $request, $id)
    {

        $orders = Order::find($id);
        $orders->status_bayar    = $request->get('status_bayar');
        $orders->konfirmasi_bayar = 'confirmed';
        // dd($orders);

        $orders->save();
        $order_produk = OrderProduk::where('order_id',$orders->id)->get();
        // dd($order_produk);
        foreach ($order_produk as $val) {
           $produk = Produk::find($val->produk_id);
           $produk->stok = $produk->stok - $val->quantity;
           $produk->save();
        }
        Alert::success('Status Bayar berhasil diubah !', 'Success');

        return redirect('orders/order_view/' . $id . '')->with('success', 'Status Bayar sukses di update');
    }

    public function konfirmasi_pembayaran(Request $request, $id) { ##customer
        $orders = Order::find($id);
        $orders->konfirmasi_bayar = $request->get('status_bayar');

        // dd($orders);
        $orders->save();
        Alert::success('Status Bayar berhasil diubah !', 'Success');

        return redirect('frontend/produk-list')->with('success', 'Status Bayar sukses di update');
    }

    public function generateInvoice($id) {
        //GET DATA BERDASARKAN ID
        $orders = Order::with(['alamat', 'order_status', 'kurir'])->where('orders.id', '=', $id)->first();
        $order_produks = OrderProduk::where('order_id',$orders->id)->get();
        //LOAD PDF YANG MERUJUK KE VIEW PRINT.BLADE.PHP DENGAN MENGIRIMKAN DATA DARI INVOICE
        //KEMUDIAN MENGGUNAKAN PENGATURAN LANDSCAPE A4
        $pdf = PDF::loadView('order.print', compact('orders','order_produks'))->setPaper('a4', 'landscape');
        // dd($pdf->stream());
        return $pdf->stream();
    }




}
