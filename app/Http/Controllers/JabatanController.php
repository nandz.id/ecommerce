<?php

namespace App\Http\Controllers;

use Alert;
use Illuminate\Http\Request;
use App\Models\Jabatan;
use App\Models\Divisi;
use Datatables;
use DB;
use Form;
use App\DataTables\UsersDataTable;
use App\Transformers\JabatanTransformer;



class JabatanController extends Controller
{
    public function __construct() {
        // $this->middleware('auth');
        $this->class_error = 'has-error';
    }

    public function index() {

        $jabatan = Jabatan::all();
        return view('jabatan.index', compact('jabatan'));

        // $jabatan = DB::table('jabatan as j')
        //     ->leftJoin('divisi as d', 'j.divisi_id', '=', 'd.id')
        //     ->select(['j.id', 'j.nama_jabatan', 'd.nama_divisi', 'j.keterangan'])
        //     ->orderBy('j.id', 'asc')
        //     ->get();

    	// return fractal()
    	// 	->collection($jabatan)
    	// 	->transformWith(new JabatanTransformer)
    	// 	->toArray();

    }

    // public function index(UsersDataTable $dataTable)
    // {
    //     return $dataTable->render('jabatan.coba');
    // }

    public function jabatanList()
    {
        $jabatan = DB::table('jabatan as j')
            ->leftJoin('divisi as d', 'j.divisi_id', '=', 'd.id')
            ->select(['j.id', 'j.nama_jabatan', 'd.nama_divisi', 'j.keterangan'])
            ->orderBy('j.id', 'asc')
            ->get();

        return datatables()->of($jabatan)
            ->addIndexColumn()
            ->addColumn('action', function ($jabatan) {
                return '<form action='.action('JabatanController@destroy', $jabatan->id).'" method="post">
                <a href="'.action('JabatanController@edit', $jabatan->id).'" class="btn btn-sm btn-success " data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
                '.Form::token().'
                <input name="_method" type="hidden" value="DELETE">
                <button class="btn btn-sm btn-danger" data-toggle="tooltip" title="Edit" type="submit"><i class="fa fa-trash"></i></button>
              </form>';
            })
            ->editColumn('id', 'ID: {{$id}}')
            ->make(true);

        // $jabatan = DB::table('jabatan as j')
        //                 ->leftJoin('divisi as d', 'j.divisi_id', '=', 'd.id')
        //                 ->select(['j.id as id', 'j.nama_jabatan as nama_jabatan', 'd.nama_divisi as nama_divisi', 'j.keterangan as keterangan'])
        //                 ->orderBy('j.id', 'asc')
        //                 ->get();
        // return datatables()->of($jabatan)
        //         ->make(true);


    }

    public function create() {

        $divisi = Divisi::all();
        $class_error = $this->class_error;
        return view('jabatan.create',compact('divisi','class_error'));
    }

    public function store (Request $request) {

        $this->validate($request, [
            'nama_jabatan' => 'required|string|max: 50',
            'divisi_id'    => 'required',
            'keterangan'   => 'required|string|max: 255'
        ],
        [
            'nama_jabatan.required' => 'Nama Jabatan wajib diisi !',
            'divisi_id.required'    => 'Divisi wajib dipilih !',
            'keterangan.required'   => 'Keterangan wajib diisi !'
        ]);

        $jabatan = new Jabatan;
        $jabatan->nama_jabatan = $request->nama_jabatan;
        $jabatan->divisi_id    = $request->divisi_id;
        $jabatan->keterangan   = $request->keterangan;
        $jabatan->save();
        Alert::success('Data Jabatan berhasil ditambahkan !','Success');
        return redirect('jabatan')->with('Success','Data Jabatan berhasil ditambahkan !');

    }

    public function edit($id) {

        $jabatan = Jabatan::find($id);
        $divisi = Divisi::all();
        $class_error = $this->class_error;
        return view('jabatan.edit', compact('jabatan','divisi','class_error', 'id'));

    }

    public function update (Request $request, $id) {

        $this->validate($request, [
            'nama_jabatan' => 'required|string|max: 50',
            'divisi_id'    => 'required',
            'keterangan'   => 'required|string|max: 255'
        ],
        [
            'nama_jabatan.required' => 'Nama Jabatan wajib diisi !',
            'divisi_id.required' => 'Divisi wajib dipilih !',
            'keterangan.required'  => 'Keterangan wajib diisi !'
        ]);

        $jabatan = Jabatan::find($id);
        $jabatan->nama_jabatan = $request->nama_jabatan;
        $jabatan->divisi_id    = $request->divisi_id;
        $jabatan->keterangan   = $request->keterangan;
        $jabatan->save();
        Alert::success('Data Jabatan berhasil diubah !','Success');

        return redirect('jabatan')->with('Success', 'Data Jabatan berhasil diubah !');
    }

    public function destroy ($id) {
        $jabatan = Jabatan::find($id);
        $jabatan->delete();
        Alert::error('Data Jabatan berhasil hapus !','Success');
        return redirect('jabatan')->with('Success', 'Data berhasil di delete');
    }

    public function laporanExcel()
    {
        return Excel::download(new DivisiReport, 'divisi.xlsx');
    }







}
