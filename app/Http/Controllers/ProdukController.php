<?php

namespace App\Http\Controllers;
// namespace App\Models;

use App\Http\Controllers\Controller;
use Alert;
use Datatables;
use App\Models\Produk;
use App\Models\ProdukGambar;
use App\Models\Kategori;
use App\Models\Jabatan;
use DB, Response, Config;
use Illuminate\Http\Request;
use App\Events\ProdukWasCreated;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;


class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function __construct () {
        $this->class_error ="has-error";
        $this->path = storage_path('app/public/img');
        //DEFINISIKAN DIMENSI
        $this->dimensions = ['245', '300', '500'];


    }

    public function index()
    {

        $produk = Produk::all();
        // die();
        return view ('produk.index',compact('produk'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sku = autonumber(Produk::getTableName(),'sku','SKU',8);
        $kategoris = Kategori::all();
        $produk_id = DB::table('produk')->latest('id')->first();
        $class_error = $this->class_error;
        return view('produk.create',compact('class_error','sku','kategoris','produk_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(Request $request)
    {

        $this->validate($request, [
            'nama'        => 'required|string|max: 255',
            'kategori_id' => 'required',
            'deskripsi'   => 'required',
            'harga'       => 'required|numeric|not_in:0',
            'stok'        => 'required|numeric',
            'active'      => 'required|int',

        ]);
        $produk = new Produk;
        $produk->kategori_id = $request->get('kategori_id');
        $produk->sku         = $request->get('sku');
        $produk->nama        = $request->get('nama');
        $produk->deskripsi   = $request->get('deskripsi');
        $produk->harga       = $request->get('harga');
        $produk->stok        = $request->get('stok');
        $produk->active      = $request->get('active');
        $produk->save();

        Alert::success('Data Produk berhasil ditambah !','Success');

        return redirect('produk')->with('success', 'Data Produk telah ditambahkan');


        // try{

        //    DB::beginTransaction();

        //     $produk = request()->all();
        //     $produk['foto'] = $fileName;
        //     $event_produk = Produk::create($produk);

        //     event(new ProdukWasCreated($event_produk));

        //     DB::commit();

        //     Alert::success('Data Produk berhasil ditambah !','Success');

        //     return redirect('Produk')->with('success', 'Data Produk telah ditambahkan');
        // } catch (Exeption $e) {

        //     DB::rollback();
        //     return $e;
        // }
    }

    public function postImages(Request $request)
    {

        if ($request->ajax()) {

            if ($request->hasFile('file')) {
                $imageFiles = $request->file('file');
                // set destination path
                $folderDir = 'uploads/products';
                $destinationPath = public_path() . '/' . $folderDir;


                // this form uploads multiple files
                foreach ($imageFiles as $fileKey => $fileObject ) {
                    // make sure each file is valid
                    if ($fileObject->isValid()) {
                        // make destination file name
                        $destinationFileName = Carbon::now()->timestamp . '_' . uniqid() . '.' . $fileObject->getClientOriginalExtension();
                        // move the file from tmp to the destination path
                        $fileObject->move($destinationPath, $destinationFileName);
                        // save the the destination filename
                        $productImage = new ProdukGambar;
                        $productImage->nama = $destinationFileName;

                        $productImage->produk_id = $request->produk_id;
                        $productImage->save();

                    }
                }

                $images = ProdukGambar::where('produk_id', $request->produk_id)->get()->toArray();

                return response()->json($images);
            }
        }
    }

    public function removeImages(Request $request)
    {
        $folderDir = 'uploads/products';
        $id = $request->id;
        $uploaded_image = ProdukGambar::where('id', $id)->first();

        if (empty($uploaded_image)) {
            return Response::json(['message' => 'Sorry file does not exist'], 400);
        }

        $file_path = public_path() . '/' .$folderDir .'/' . $uploaded_image->nama;

        if (file_exists($file_path)) {
            unlink($file_path);
        }

        if (!empty($uploaded_image)) {
            $uploaded_image->delete();
        }

        return response()->json(['success' => true, 'message' => 'gambar produk berhasil di hapus !']);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
        // public function show($id)
        // {
        //     $Produk = Produk::find($id);
        //     $jabatan  = Jabatan::all();
        //     $class_error = $this->class_error;
        //     return view('Produk.show',compact('Produk','jabatan','class_error', 'id'));
        // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {

        $produk = Produk::find($id);
        $kategoris = Kategori::all();
        $produk_gambars = ProdukGambar::where('produk_id',$produk->id)->get();

        $class_error = $this->class_error;
    return view('produk.edit',compact('produk','produk_gambars','class_error', 'id','kategoris'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama'      => 'required|string|max: 255',
            'deskripsi' => 'required',
            'harga'     => 'required|not_in:0|numeric',
            'stok'      => 'required|numeric',
            'active'    => 'required|int',

        ]);

        $produk = Produk::find($id);
        $produk->nama      = $request->get('nama');
        $produk->deskripsi = $request->get('deskripsi');
        $produk->harga     = $request->get('harga');
        $produk->stok      = $request->get('stok');
        $produk->active    = $request->get('active');

        // print_r($Produk);
        // die();
        $produk->save();
        Alert::success('Data Produk berhasil diubah !','Success');

        return redirect('produk')->with('success', 'Data Produk sukses di update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produk = Produk::find($id);
        $produk->delete();

        return redirect('produk')->with('success','Data Produk telah di hapus');
    }


}
