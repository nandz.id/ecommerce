<?php

namespace App\Http\Controllers;

use Alert;
use Illuminate\Http\Request;
use App\Models\OrderStatus;
use Datatables;
use DB;
use Form;
use App\DataTables\OrderStatusDataTable;




class OrderStatusController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
        $this->class_error = 'has-error';
    }

    public function index()
    {

        $order_status = OrderStatus::all();
        return view('order_status.index', compact('order_status'));
    }

    public function order_status_list()
    {
        $order_status = OrderStatus::all();

        return datatables()->of($order_status)
            ->addIndexColumn()
            ->addColumn('action', function ( $order_status) {
                return '<div class="row">
                            <div class="col-md-6 text-right" style="padding-left:0px;padding-right:0px">
                                <form class="delete_form" action=' . action('OrderStatusController@destroy', $order_status->id) . '" method="post">
                                    <a href="' . action('OrderStatusController@edit', $order_status->id) . '" class="btn btn-sm btn-success test123" data-toggle="tooltip" title="Edit"><i class="fa fa-pencil"></i></a>
                                    ' . Form::token() . '
                                    <input name="_method" type="hidden" value="DELETE">
                                    <input type="submit" name="submit" class="submit" id="hapus_' . $order_status->id . '" style="display:none;">
                                </form>
                            </div>
                            <div class="col-md-6 text-left" style="padding-left:0px;padding-right:0px">
                                <button class="btn btn-sm btn-danger klik" data-toggle="tooltip" value-id ="' . $order_status->id . '" title="Edit"> <i class="fa fa-trash"></i></button>
                            </div>
                        </div>';
            })
            ->editColumn('id', 'ID: {{$id}}')
            ->make(true);
    }

    public function create()
    {
        $class_error = $this->class_error;
        return view('order_status.create', compact('class_error'));
    }

    public function store(Request $request)
    {

        $this->validate(
            $request,
            [
                'nama'       => 'required|string|max: 100',
                'notifikasi' => 'required|string|max: 2'
            ],
            [
                'nama.required' => 'Nama Order Status wajib diisi !',
                'notifikasi.required' => 'Notifikasi wajib dipilih'
            ]
        );

        $order_status = new OrderStatus;
        $order_status->nama       = $request->nama;
        $order_status->notifikasi = $request->notifikasi;

        $order_status->save();
        Alert::success('Data Order Status berhasil ditambahkan !', 'Success');
        return redirect('order_status')->with('Success', 'Data Order Status berhasil ditambahkan !');
    }

    public function edit($id)
    {

        $order_status = OrderStatus::find($id);
        $class_error = $this->class_error;
        return view('order_status.edit', compact('order_status', 'class_error', 'id'));
    }

    public function update(Request $request, $id)
    {

        $this->validate(
            $request,
            [
                'nama'       => 'required|string|max: 100',
                'notifikasi' => 'required|string|max: 2'
            ],
            [
                'nama.required' => 'Nama Order Status wajib diisi !',
                'notifikasi.required' => 'Notifikasi wajib dipilih'
            ]
        );

        $order_status = OrderStatus::find($id);
        $order_status->nama       = $request->nama;
        $order_status->notifikasi = $request->notifikasi;
        $order_status->save();
        Alert::success( 'Data Order Status berhasil diubah !', 'Success');

        return redirect('order_status')->with('Success', 'Data Order Status berhasil diubah !');
    }

    public function destroy($id)
    {
        $order_status = OrderStatus::find($id);
        $order_status->delete();
        Alert::success( 'Data Order Status berhasil hapus !', 'Success');
        return redirect('order_status')->with('Success', 'Data berhasil di delete');
    }

    public function laporanExcel()
    {
        return Excel::download(new OrderStatusReport, 'OrderStatus.xlsx');
    }
}
