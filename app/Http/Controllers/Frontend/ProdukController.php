<?php

namespace App\Http\Controllers\Frontend;
// namespace App\Models;

use DB;
use Alert;
use Notification;
use App\Models\Produk;
use App\Models\Kategori;
use Darryldecode\Cart\Cart;
use App\Models\ProdukGambar;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Darryldecode\Cart\CartCondition;
use Illuminate\Support\Facades\Auth;
use App\Notifications\MyFirstNotification;

class ProdukController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->paginate = 10;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


    public function index()
    {
        $data           = $this->show_produks();
        $details_produk = $data['details_produk'];
        $total_produk   = $data['total_produk'];
        $kategoris      = Kategori::all();
        $kategori_judul = $data['kategori_judul'];

        return view('frontend.produk.produk_list', compact('kategoris','kategori_judul','details_produk','total_produk'));
    }

    public function produk_list($slug) {

        $data           = $this->show_produks($slug);
        $kategoris      = Kategori::all();
        $total_produk   = $data['total_produk'];
        $details_produk = $data['details_produk'];
        $kategori_judul = $data['kategori_judul'];
        return view('frontend.produk.produk_list', compact('kategoris','kategori_judul','details_produk','total_produk','slug'));

    }

    public function produk_search(Request $request) {

        $kategoris      = Kategori::all();
        $data           = $this->show_produks('',$request->get('search'));
        $total_produk   = $data['total_produk'];
        $details_produk = $data['details_produk'];
        // dd($details_produk);
        $kategori_judul = $data['kategori_judul'];

        return view('frontend.produk.produk_list', compact('kategoris','kategori_judul','details_produk','total_produk','nama_produk'));
    }

    public function show_produks($slug ='',$nama_produk='') {

        $kategoris      = Kategori::all();
        $kategori_judul = 'Semua Produk';
        $total_produk   = Produk::where('active',1)->count();
        // dd($kategoris);
        $details_produk = $this->details_produk('',$this->paginate);


        if($slug != '') {
            $kategoris      = Kategori::where('slug', $slug)->first()->toArray();
            $details_produk = $this->details_produk($kategoris['id'],$this->paginate);
            $query          = Produk  ::where('kategori_id',$kategoris['id']);
            $total_produk   = $query->where('active',1)->count();
            $kategori_judul = $kategoris['nama'];

        }

        if($nama_produk != '') {
            $details_produk = $this->details_produk('', $this->paginate,$nama_produk);
            // dd($details_produk);
            $query          = $details_produk;
            $total_produk   = $query->where('active',1)->count();

        }

        // dd($details_produk);
        $data = array();
        $data['kategori_judul'] = $kategori_judul;
        $data['details_produk'] = $details_produk;
        $data['total_produk']   = $total_produk;

        return $data;

    }

    public function details_produk($kategori_id='', $paginate, $nama_produk='') {

        $query = DB::table('produk as p')
                ->leftJoin('kategori as k', 'k.id', '=', 'p.kategori_id')
                ->select('p.*','k.nama as nama_kategori',
                    DB::raw("(select nama as nama_gambar from produk_gambar pg where pg.produk_id = p.id limit 1) as gambar_produk"));
                if($kategori_id != '') {
                    $details_produk = $query->where('kategori_id','=',$kategori_id);
                }
                if($nama_produk != '') {
                    $details_produk = $query->where('p.nama', 'LIKE', "%{$nama_produk}%");
                }
                $details_produk = $query->where('p.active','=',1);
                $details_produk = $query->orderBy('p.id', 'asc');
                $details_produk = $query->paginate($paginate);

        return $details_produk;
    }

    public function produk_detail($id) {

        $kategoris      = Kategori::all();
        $produk_details = DB::table('produk as p')
                        ->leftJoin('kategori as k', 'k.id', '=', 'p.kategori_id')
                        ->select('p.*','k.nama as nama_kategori')
                        ->where('p.id',$id)
                        ->first();
        // dd($produk_details);
        $produk_gambars = ProdukGambar::where('produk_id',$produk_details->id)->get()->toArray();

        $gambar_selected ='no_image.png';
        if ($produk_gambars != null) {
            $gambar_selected = $produk_gambars[0]['nama'];
        }

        return view('frontend.produk.produk_detail', compact('produk_details','produk_gambars','gambar_selected','kategoris'));
        // dd($gambar_selected);
    }

    public function add_cart(Request $request)
    {
        if ($request->ajax()) {

            $userId = 1; // get this from session or wherever it came from

            $produk_id = $request->get('id');
            $nama      = $request->get('nama');
            $harga     = $request->get('harga');
            $qty       = $request->get('qty');


            $customAttributes = [
                'gambar'    => $request->get('gambar'),
                'color_attr' => [
                    'label' => 'red',
                    'price' => 10.00,
                ],
                'size_attr' => [
                    'label' => 'xxl',
                    'price' => 15.00,
                ]
            ];

            $item = \Cart::session($userId)->add($produk_id, $nama, $harga, $qty ,$customAttributes);


            return response(array(
                'success' => true,
                'data' => $item,
                'message' => "item added."
            ), 201, []);
        }
    }

    public function produk_cart() {

        $userId = 1;
        $items = [];

        \Cart::session($userId)->getContent()->each(function ($item) use (&$items) {
            $items[] = $item;
        });



        $total_quantity = \Cart::session($userId)->getTotalQuantity();
        $total = \Cart::session($userId)->getTotal();

        return view('frontend.produk.produk_cart',compact('items', 'total_quantity', 'total'));

    }

    public function update_quantity_cart(Request $request)
    {

        $userId = 1;
        $items = [];

        \Cart::session($userId)->getContent()->each(function ($item) use (&$items) {
            $items[] = $item;
        });
        foreach ($items as $key=> $val) {
            \Cart::update($val->id, array(
                'quantity' => (-$val['quantity']) + $request->input('quantity_input_'.$key.'')
            ));
        }

        return redirect('/frontend/checkout');
    }



    public function details()
    {
        $userId = 1; // get this from session or wherever it came from

        // get subtotal applied condition amount
        $conditions = \Cart::session($userId)->getConditions();


        // get conditions that are applied to cart sub totals
        $subTotalConditions = $conditions->filter(function (CartCondition $condition) {
            return $condition->getTarget() == 'subtotal';
        })->map(function (CartCondition $c) use ($userId) {
            return [
                'name' => $c->getName(),
                'type' => $c->getType(),
                'target' => $c->getTarget(),
                'value' => $c->getValue(),
            ];
        });

        // get conditions that are applied to cart totals
        $totalConditions = $conditions->filter(function (CartCondition $condition) {
            return $condition->getTarget() == 'total';
        })->map(function (CartCondition $c) {
            return [
                'name' => $c->getName(),
                'type' => $c->getType(),
                'target' => $c->getTarget(),
                'value' => $c->getValue(),
            ];
        });

        return response(array(
            'success' => true,
            'data' => array(
                'total_quantity' => \Cart::session($userId)->getTotalQuantity(),
                'sub_total' => \Cart::session($userId)->getSubTotal(),
                'total' => \Cart::session($userId)->getTotal(),
                'cart_sub_total_conditions_count' => $subTotalConditions->count(),
                'cart_total_conditions_count' => $totalConditions->count(),
            ),
            'message' => "Get cart details success."
        ), 200, []);
    }

    public function clear_cart()
    {

        $userId = 1;

        \Cart::clear();
        \Cart::session($userId)->clear();

        Alert::success('Keranjang berhasil di Clear !', 'Success');

        return redirect()->route('produk_cart');

    }

    public function delete_item($id)
    {
        $userId = 1; // get this from session or wherever it came from

        \Cart::session($userId)->remove($id);

        return response(array(
            'success' => true,
            'data' => $id,
            'message' => "Item {$id} berhasil di hapus dari keranjang."
        ), 200, []);
    }

}
