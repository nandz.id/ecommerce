<?php

namespace App\Http\Controllers\Frontend;
// namespace App\Models;

use DB;
use PDF;
use Alert;
use Notification;
use App\Models\Kurir;
use App\Models\Order;
use App\Models\Alamat;
use App\Models\Produk;
use App\Models\OrderProduk;
use Darryldecode\Cart\Cart;
use Illuminate\Http\Request;
use App\Events\OrderWasCreated;
use App\Models\OrderStatusHistory;
use App\Http\Controllers\Controller;
use Darryldecode\Cart\CartCondition;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\RedirectResponse;
use App\Notifications\MyFirstNotification;

class CheckoutController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


    public function index()
    {
        $userId = 1; // get this from session or wherever it came from

        $kurirs = Kurir::all();
        $items = content_cart($userId);
        // dd($items);

        $total_quantity = \Cart::session($userId)->getTotalQuantity();
        $total = \Cart::session($userId)->getTotal();

        return view('frontend.produk.produk_checkout',compact('items','total_quantity','total','kurirs','produks'));
    }

    public function order_simpan(Request $request) {

        $this->validate($request, [

            'nama_depan'      => 'required|string|max: 200',
            'nama_belakang'   => 'required|string|max: 200',
            'alamat_lengkap'  => 'required|string|max: 255',
            'email'           => 'required|string|max:100',
            'kota'            => 'required|string|max: 100',
            'kode_pos'        => 'required|string|max: 15',
            'no_handphone'    => 'required|string|max: 15',
            'kurir_id'        => 'required|int|max: 10',
            'payment_method'  => 'required|string|max: 255',
            'total_produk'    => 'required|numeric',
            'total'           => 'required|numeric'

        ]);

        try{

            DB::beginTransaction();

            $id_user = 1;

            $alamat = new Alamat;
            $alamat->user_id        = $id_user;
            $alamat->nama_depan     = $request->nama_depan;
            $alamat->nama_belakang  = $request->nama_belakang;
            $alamat->alamat_lengkap = $request->alamat_lengkap;
            $alamat->kota           = $request->kota;
            $alamat->kode_pos       = $request->kode_pos;
            $alamat->no_handphone   = $request->no_handphone;
            $alamat->save();

            $kurir = Kurir::find($request->kurir_id)->first();


            $invoice_number = autonumber(Order::getTableName(), 'invoice_no', 'INV', 7);
            $order_number = autonumber(Order::getTableName(), 'order_kode', 'ODR', 9);
            $order = new Order;
            $order->order_kode      = $order_number;
            $order->kurir_id        = $request->kurir_id;
            $order->user_id         = $id_user;
            $order->order_status_id = 1; //pending
            $order->alamat_id       = $alamat->id;
            $order->email           = $request->email;
            $order->total_produk    = $request->total_produk;
            $order->total_shipping  = $kurir->harga;
            $order->payment_method  = $request->payment_method;
            $order->invoice_no      = $invoice_number;
            $order->invoice_date    = date('Y-m-d');
            $order->total           = $request->total;
            $order->delivery_date   = date('Y-m-d', strtotime($order->invoice_date . " +3 days"));
            $order->save();

            $order_sh = new OrderStatusHistory;
            $order_sh->order_id = $order->id;
            $order_sh->order_status_id = 1; //pending
            $order_sh->save();

            $items = content_cart($id_user);
            // dd($items);
            foreach ($items as $key => $item) {
                $order_produks = new OrderProduk;
                $order_produks->order_id         = $order->id;
                $order_produks->produk_id        = $item->id;
                $order_produks->produk_harga     = $item->price;
                $order_produks->quantity         = $item->quantity;
                $order_produks->produk_nama      = $item->produk_nama;
                $order_produks->produk_sku       = $item->produk_sku;
                $order_produks->produk_deskripsi = $item->produk_deskripsi;
                $order_produks->save();

            }

            DB::commit();

            $orders = Order::with(['alamat', 'order_status', 'kurir'])->where('orders.id', '=', $order->id)->first();
            $order_produks = OrderProduk::where('order_id', $orders->id)->get();
            //LOAD PDF YANG MERUJUK KE VIEW PRINT.BLADE.PHP DENGAN MENGIRIMKAN DATA DARI INVOICE
            //KEMUDIAN MENGGUNAKAN PENGATURAN LANDSCAPE A4
            $pdf = PDF::loadView('order.print', compact('orders', 'order_produks'))->setPaper('a4', 'landscape');
            event(new OrderWasCreated($order, $items, $pdf));

            \Cart::clear();
            \Cart::session($id_user)->clear();

            Alert::success('Data Produk berhasil ditambah !','Success');
            return redirect('/frontend/order_sukses/'.$order->order_kode.'');
        } catch (Exeption $e) {

            DB::rollback();
            Alert::error('ERROR', 'Success');
            return $e;
        }

    }

    public function order_sukses($order_kode) {

        $orders = DB::table('orders as o')
                    ->leftJoin('alamat as a', 'a.id', '=', 'o.alamat_id')
                    ->select('a.nama_depan','a.nama_belakang', 'o.email')
                    ->where('o.order_kode', $order_kode)
                    ->first();
        // dd($orders);

        return view('frontend.produk.produk_order_sukses',compact('orders'));
    }


}
