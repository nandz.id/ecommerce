<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProdukGambar extends Model {

    protected $table = 'produk_gambar';
    protected $fillable = [
        'id',
        'produk_id',
        'nama'

    ];

    public $timestamps = false;

    public function produk() {
        return $this->belongsTo('App\Models\Produk');
    }


}
