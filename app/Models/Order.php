<?php

namespace App\Models;

use App\Mail\OrderProdukCreated;
use Illuminate\Support\Facades\Mail;
use Illuminate\Database\Eloquent\Model;
use DB;
class Order extends Model
{
    protected $table ='orders';

    protected $fillable = [
        'id',
        'kurir_id',
        'user_id',
        'order_status_id',
        'alamat_id',
        'payment_method',
        'diskon',
        'total_produk',
        'total_shipping',
        'invoice_no',
        'invoice_date',
        'total',
        'status_bayar',
        'konfirmasi_bayar',
        'delivery_date',
        'created_at',
        'updated_at'

    ];


    public static function getTableName()
    {
        return with(new static)->getTable();
    }


    public function kurir() {
        return $this->belongsTo('App\Models\Kurir');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function order_status() {
        return $this->belongsTo('App\Models\OrderStatus');
    }

    public function alamat() {
        return $this->belongsTo('App\Models\Alamat');
    }

    public function order_produk() {
        return $this->hasMany('App\Models\OrderProduk');
    }


}
