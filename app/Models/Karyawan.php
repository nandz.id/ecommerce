<?php

namespace App\Models;

use App\Mail\KaryawanCreated;
use Illuminate\Support\Facades\Mail;
use Illuminate\Database\Eloquent\Model;
use DB;
class Karyawan extends Model
{
    protected $table ='karyawan';
    // protected $primaryKey = 'nik';
    protected $prefix = 'PGT';
    protected $fillable = [
        'nik',
        'nama_lengkap',
        'tempat_lahir',
        'tanggal_lahir',
        'jk',
        'jabatan_id',
        'email',
        'alamat',
        'foto'
    ];

    protected $guarded = [
        'foto'
    ];

    public static function getTableName()
    {
        return with(new static)->getTable();
    }

    // public static function autonumber($table = 'karyawan', $primary = 'nik', $prefix = 'PGT'){
    //     $create_nik=DB::table($table)->select(DB::raw('MAX(RIGHT('.$primary.',4)) as kode_nik'))->get();

    //     if($create_nik->count()>0)
    //     {
    //         foreach($create_nik as $k)
    //         {
    //             $tmp = ((int)$k->kode_nik)+1;
    //             $kode = $prefix.sprintf("%04s", $tmp);
    //         }
    //     }
    //     else
    //     {
    //         $kode = $prefix."00001";
    //     }

    //     return $kode;
    // }

    // public function jabatan() {
    //     return $this->belongsTo('App\Models\Jabatan');
    // }

}
