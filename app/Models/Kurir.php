<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kurir extends Model {

    protected $table = 'kurir';
    protected $fillable = [
        'id',
        'nama',
        'harga',
        'deskripsi',
        'logo'
    ];
    public $timestamps = false;

    public function order()
    {
        return $this->hasOne('App\Models\Order');
    }





}
