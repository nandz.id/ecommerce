<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderStatusHistory extends Model {

    protected $table = 'order_status_history';
    protected $fillable = [
        'order_id',
        'order_status_id',
    ];

    public function order_status()
    {

        return $this->belongsTo('App\Models\OrderStatus');
    }




}
