<?php

namespace App\Models;

use App\Mail\ProdukCreated;
use Illuminate\Support\Facades\Mail;
use Illuminate\Database\Eloquent\Model;
use DB;
class Produk extends Model
{
    protected $table ='produk';

    protected $fillable = [
        'id',
        'sku',
        'nama',
        'deskripsi',
        'harga',
        'stok',
        'active',

    ];


    public static function getTableName()
    {
        return with(new static)->getTable();
    }


    public function kategori() {
        return $this->belongsTo('App\Models\Kategori');
    }

    public function produk_gambar() {
        return $this->hasMany('App\Models\ProdukGambar', 'sku', 'sku_id');
    }







}
