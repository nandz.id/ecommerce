<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model {

    protected $table = 'order_status';
    protected $fillable = [
        'id',
        'nama',
    ];
    public $timestamps = false;


    public function order() {

        return $this->hasMany('App\Models\Order');
    }

    public function order_status_history()
    {

        return $this->hasMany('App\Models\OrderStatusHistory');
    }




}
