<?php

namespace App\Models;

use App\Mail\AlamatCreated;
use Illuminate\Support\Facades\Mail;
use Illuminate\Database\Eloquent\Model;
use DB;
class Alamat extends Model
{
    protected $table ='alamat';

    protected $fillable = [
        'user_id',
        'nama_depan',
        'nama_belakang',
        'alamat_lengkap',
        'kota',
        'kode_pos',
        'telepon',
        'no_handphone',
        'created_at',
        'updated_at'

    ];


    public static function getTableName()
    {
        return with(new static)->getTable();
    }


    public function user() {
        return $this->belongsTo('App\User');
    }

    public function order() {
        return $this->hasOne('App\Models\Order');
    }









}
