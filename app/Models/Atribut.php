<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Atribut extends Model {

    protected $table = 'atribut';
    protected $fillable = [
        'tipe',
        'nama',
    ];
    public $timestamps = false;


}
