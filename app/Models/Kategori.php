<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model {

    protected $table = 'kategori';
    protected $fillable = [
        'id',
        'parent_id',
        'nama',
        'slug',
        'keterangan',
        'lft',
    	'rgt',
    	'depth'
    ];
    public $timestamps = false;


    public function produk() {

        return $this->hasMany('App\Models\Produk');
    }



}
