<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class AtributValue extends Model
{


    protected $table = 'atribut_value';
    //protected $primaryKey = 'id';
    public $timestamps = false;
    // protected $guarded = ['id'];
    protected $fillable = [
    	'atribut_id',
    	'value'
	];



}
