<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use DB;
class OrderProduk extends Model
{
    protected $table ='order_produk';

    protected $fillable = [
        'order_id',
        'produk_id',
        'quantity',
        'produk_nama',
        'produk_sku',
        'produk_deskripsi',
        'produk_harga'

    ];


    public static function getTableName()
    {
        return with(new static)->getTable();
    }


    public function produk() {
        return $this->belongsTo('App\Models\Produk');
    }

    public function order() {
        return $this->belongsTo('App\Models\Order');
    }


}
