<?php

namespace App\Events;

// use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
// use Illuminate\Broadcasting\PrivateChannel;
// use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
// use Illuminate\Broadcasting\InteractsWithSockets;
// use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class OrderWasCreated
{
    use Dispatchable, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */

    public $order;
    public $order_produks;
    public $pdf;
    public $alamat;

    public function __construct($order,$order_produks,$pdf)
    {
        $this->order = $order;
        $this->order_produks = $order_produks;
        $this->pdf = $pdf;
    }



}
