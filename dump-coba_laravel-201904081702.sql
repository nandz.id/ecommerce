-- MySQL dump 10.13  Distrib 5.7.25, for Linux (x86_64)
--
-- Host: localhost    Database: shop_test
-- ------------------------------------------------------
-- Server version	5.7.25-0ubuntu0.18.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `divisi`
--

DROP TABLE IF EXISTS `divisi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `divisi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_divisi` varchar(50) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `divisi`
--

LOCK TABLES `divisi` WRITE;
/*!40000 ALTER TABLE `divisi` DISABLE KEYS */;
INSERT INTO `divisi` VALUES (1,'IT','upup asfasf',NULL,'2019-04-02 04:16:32'),(2,'HRD',NULL,NULL,NULL),(3,'FINANCE','asfasf',NULL,'2019-04-02 04:16:15'),(6,'TEST','aaa asfasf','2019-03-29 08:25:28','2019-04-02 04:16:10'),(7,'asdasd','aaaa','2019-04-05 08:01:29','2019-04-05 08:01:29');
/*!40000 ALTER TABLE `divisi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jabatan`
--

DROP TABLE IF EXISTS `jabatan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jabatan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `divisi_id` int(11) DEFAULT NULL,
  `nama_jabatan` varchar(50) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jabatan`
--

LOCK TABLES `jabatan` WRITE;
/*!40000 ALTER TABLE `jabatan` DISABLE KEYS */;
INSERT INTO `jabatan` VALUES (1,7,'asdasd','asfasf','2019-04-05 08:02:46','2019-04-05 08:02:46'),(2,7,'asdasd','qwfqwf','2019-04-05 08:05:58','2019-04-05 08:05:58'),(3,3,'cobacoasd','asfasf','2019-04-05 08:31:47','2019-04-05 08:31:47'),(4,1,'asfasf','asfasf','2019-04-05 09:03:26','2019-04-05 09:03:26'),(5,1,'asdasd','asdasd','2019-04-05 09:11:53','2019-04-05 09:11:53'),(6,1,'asfasf','asfas','2019-04-05 09:12:52','2019-04-05 09:12:52'),(7,2,'dsfgsdg','sdgsdg','2019-04-05 09:13:26','2019-04-05 09:13:26'),(8,1,'asfasf','asgasg','2019-04-05 09:21:01','2019-04-05 09:21:01');
/*!40000 ALTER TABLE `jabatan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `karyawan`
--

DROP TABLE IF EXISTS `karyawan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `karyawan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nik` varchar(10) DEFAULT NULL,
  `nama_lengkap` varchar(255) DEFAULT NULL,
  `tempat_lahir` varchar(50) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `jk` enum('L','P') DEFAULT NULL,
  `jabatan_id` int(11) DEFAULT NULL,
  `alamat` text,
  `foto` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `karyawan`
--

LOCK TABLES `karyawan` WRITE;
/*!40000 ALTER TABLE `karyawan` DISABLE KEYS */;
INSERT INTO `karyawan` VALUES (2,'PGT0001','Rusby','Cirebon','1993-04-14','L',3,'Tanjung Priok','1554174168_5ca2d0d8910e8.png',NULL,'2019-04-02 03:09:29'),(3,'PGT0002','Hilman','Cirebon','1993-04-14','P',3,'Kemayoran','1554174188_5ca2d0ec2affd.png',NULL,'2019-04-02 03:12:24'),(4,'PGT0003','Aptra Aji','Cirebon','1993-04-14','L',2,'Tebet','1554174199_5ca2d0f77f8a8.png',NULL,'2019-04-02 03:08:52'),(5,'PGT0004','Johan','Jakarta','1989-02-12','L',2,'Artha Gading','1554174212_5ca2d1040f481.png','2019-03-21 08:59:54','2019-04-02 03:06:13'),(6,'PGT0005','Ilham','Tanggerang','1989-02-12','L',3,'Kelapa Gading','1554174289_5ca2d1512751e.png',NULL,'2019-04-02 03:07:12'),(7,'PGT0006','Dodi','Jakarta','1989-02-12','L',2,'Tebet','1554174338_5ca2d1828b764.jpg',NULL,'2019-04-02 03:05:39'),(8,'PGT0007','Robert','Bandung','1985-05-26','L',2,'Gedebage','1554174413_5ca2d1cd98a13.jpg','2019-03-21 08:59:54','2019-04-02 03:06:53'),(9,'PGT0008','Rahmad','Bandung','1985-05-26','L',3,'Kelapa Gading','1554174386_5ca2d1b21036d.jpg',NULL,'2019-04-02 03:06:26'),(10,'PGT0009','Riana','Bandung','1985-05-26','P',3,'Kemayoran','1554174327_5ca2d1773cdd4.jpg',NULL,'2019-04-02 03:05:27'),(12,'PGT0010','Dodi','Bandung','1985-05-26','L',2,'Tebet','1554174314_5ca2d16abb13c.png',NULL,'2019-04-02 03:05:14'),(17,'PGT0011','asfasfas','Bandung','1985-05-26','L',2,'asgasg',NULL,'2019-03-26 09:14:05','2019-03-26 09:14:05'),(18,'PGT0012','Jimmy Page','Bandung','1985-05-26','L',3,'asdas',NULL,'2019-03-26 09:16:58','2019-03-26 09:16:58'),(19,'PGT0013','asgasg','Yogyakarta','1985-05-26','P',2,'asgasg',NULL,'2019-03-26 09:21:12','2019-03-26 09:21:12'),(20,'PGT0014','atgqet3q','Yogyakarta','1985-05-26','L',1,'qrq',NULL,'2019-03-26 09:22:16','2019-03-26 09:22:16'),(21,'PGT0015','Muhammad Nanda','Yogyakarta','1985-05-26','P',2,'Jl. P Drajat\r\nGg. Semangka no 07',NULL,'2019-03-27 06:24:17','2019-03-27 06:24:17'),(27,'PGT0016','afastg','Semarang','1995-06-26','L',1,'Jl. P Drajat\r\nGg. Semangka no 07',NULL,'2019-03-27 09:57:10','2019-03-27 09:57:10'),(30,'PGT0017','gswgew','Semarang','1995-06-26','L',2,'drhdrh esryhrs',NULL,'2019-03-28 07:09:56','2019-03-28 07:09:56'),(31,'PGT0018','Rustam','Semarang','1995-06-26','L',1,'Jl. Pilang Raya No.587, Kedawung – Cirebon',NULL,'2019-03-28 07:50:43','2019-03-28 07:50:43'),(33,'PGT0019','uuuuu','Semarang','1995-06-26','P',1,'uuuuuu',NULL,'2019-03-28 08:34:35','2019-03-28 08:40:13'),(34,'PGT0020','Muhammad Nanda','Cirebon','1993-09-26','L',2,'Jl. P Drajat\r\nGg. Semangka no 07',NULL,'2019-04-01 08:00:15','2019-04-01 08:00:15'),(35,'PGT0021','Muhammad Nanda','Cirebon','1993-09-26','L',1,'Jl. P Drajat\r\nGg. Semangka no 07',NULL,'2019-04-01 08:03:54','2019-04-01 08:03:54'),(36,'PGT0022','Muhammad Nanda','Cirebon','1993-10-03','L',1,'Jl. P Drajat\r\nGg. Semangka no 07','1554108553_5ca1d089ee0ed.png','2019-04-01 08:49:14','2019-04-01 08:49:14'),(37,'PGT0023','Muhammad Nanda','Cirebon','1993-10-03','L',1,'Jl. P Drajat\r\nGg. Semangka no 07','1554108630_5ca1d0d6a8bac.png','2019-04-01 08:50:31','2019-04-01 08:50:31'),(38,'PGT0024','Muhammad Nanda','Cirebon','1993-09-26','L',1,'Jl. P Drajat\r\nGg. Semangka no 07','1554108976_5ca1d2300a6e1.png','2019-04-01 08:56:16','2019-04-01 08:56:16'),(39,'PGT0025','Muhammad Nanda','Cirebon','1993-09-26','L',1,'afafv07agagvawgv w vawfvswfsxfw vasgv','1554110738_5ca1d9126e66b.png','2019-04-01 09:25:38','2019-04-01 09:25:38'),(40,'PGT0026','asdasd','Cirebon','1993-09-26','L',2,'qfqwfqw','1554168712_5ca2bb885bada.png','2019-04-02 01:31:52','2019-04-02 01:31:52'),(41,'PGT0027','Muhammad Nanda','Cirebon','1993-09-26','P',1,'Jl. P Drajat\r\nGg. Semangka no 07','1554342583_5ca562b7c1a0f.png','2019-04-04 01:49:44','2019-04-04 01:49:44');
/*!40000 ALTER TABLE `karyawan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_03_21_043714_create_karyawans_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_has_permissions`
--

DROP TABLE IF EXISTS `model_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_type_model_id_index` (`model_type`,`model_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_has_permissions`
--

LOCK TABLES `model_has_permissions` WRITE;
/*!40000 ALTER TABLE `model_has_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `model_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `model_has_roles`
--

DROP TABLE IF EXISTS `model_has_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `model_has_roles` (
  `role_id` int(10) unsigned NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_type_model_id_index` (`model_type`,`model_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `model_has_roles`
--

LOCK TABLES `model_has_roles` WRITE;
/*!40000 ALTER TABLE `model_has_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `model_has_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
INSERT INTO `password_resets` VALUES ('nanda@merahputih.id','$2y$10$mKAPWFctFLCH9.7qiI0WdeHC.Wx5O7gTu.DJcQudYbn0VWi9c89Ry','2019-04-04 02:52:34');
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_has_permissions`
--

DROP TABLE IF EXISTS `role_has_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_has_permissions`
--

LOCK TABLES `role_has_permissions` WRITE;
/*!40000 ALTER TABLE `role_has_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_has_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'admin','web','2019-04-08 03:00:00',NULL),(2,'user','web','2019-04-08 03:00:00',NULL);
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `verified` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'Muhammad Nanda','nandz.id@gmail.com',NULL,'$2y$10$0vpRt7g.hrMcWFVAGvGdl./KI1iZPqAFxjIhFGX4k7rcjw9tzCgSS','bOfcYZuu6XsOkses9lXBrWJqkzmMTabMry4tJeCgJia58CaT8y4fcfyM5s8f','2019-03-25 00:50:56','2019-04-07 21:03:11',1),(13,2,'Nandzo','nanda@merahputih.id',NULL,'$2y$10$0M35ONJyUPkJQ14RJ5JNl.zc0NRe9tQrLlKSXaw63dzkgBjU09U4a','tNSvWlqgpEAFNRISpvTCaEwdt95dD62ppZheAQdZwg6bz8NUzS99R5M7A5Ix','2019-04-04 00:38:07','2019-04-04 01:11:13',1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `verify_users`
--

DROP TABLE IF EXISTS `verify_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `verify_users` (
  `user_id` int(11) NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `verify_users`
--

LOCK TABLES `verify_users` WRITE;
/*!40000 ALTER TABLE `verify_users` DISABLE KEYS */;
INSERT INTO `verify_users` VALUES (13,'PdWCaJ6z9dWWaBSjrCGu7Pu6uHnAFTCJZ7k5enY6','2019-04-04 00:38:07','2019-04-04 00:38:07');
/*!40000 ALTER TABLE `verify_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'shop_test'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-04-08 17:02:50
