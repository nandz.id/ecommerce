<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// use App\DataTables\UsersDataTable;

// Route::get('users', function(UsersDataTable $dataTable) {
//     return $dataTable->render('users.index');
// });

Route::get('/welcome', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/user/verify/{token}', 'Auth\RegisterController@verifyUser');

Route::get('/', 'HomeController@index')->name('home')->middleware('auth');
Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
Route::resource('karyawan','KaryawanController')->middleware('auth');
Route::resource('jabatan', 'JabatanController')->middleware('auth');
Route::resource('divisi', 'DivisiController')->middleware('auth');
Route::get('/jabatanlist','JabatanController@jabatanList');
//Excel Download
Route::get('/excel', 'DivisiController@laporanExcel');
Route::get('send', 'HomeController@sendNotification');

// Ecommerce
Route::resource('produk','ProdukController')->middleware('auth');
Route::resource('kategori','KategoriController')->middleware('auth');
Route::get('/kategorilist','KategoriController@kategoriList');
Route::resource('atribut','AtributController')->middleware('auth');
Route::get('/atributlist','AtributController@atributList');

Route::resource('order_status', 'OrderStatusController')->middleware('auth');
Route::get('/order_status_list', 'OrderStatusController@order_status_list');

Route::post('uploadImg', 'ProdukController@postImages')->name('uploadimage');
Route::post('removeImg', 'ProdukController@removeImages')->name('removeimage');

Route::resource('kurir', 'KurirController')->middleware('auth');
Route::get('orders', 'OrderController@index')->middleware('auth');
Route::get('orders/order_view/{id}', 'OrderController@view')->middleware('auth');
Route::patch('orders/update_order/{id}', 'OrderController@update_order')->name('update_order')->middleware('auth');
Route::patch('orders/update_pembayaran/{id}', 'OrderController@update_pembayaran')->name('update_pembayaran')->middleware('auth');
Route::patch('orders/konfirmasi_pembayaran/{id}', 'OrderController@konfirmasi_pembayaran')->name('konfirmasi_pembayaran')->middleware('auth');

Route::get('orders/print/{id}', 'OrderController@generateInvoice')->name('orders_print')->middleware('auth');
Route::post('update_quantity_cart', 'Frontend\ProdukController@update_quantity_cart')->name('update_quantity_cart')->middleware('auth');

// Route::post('orders/update', 'OrderController@update')->middleware('auth');





// Route front end


Route::get('/frontend', 'Frontend\ProdukController@index')->name('frontend');
Route::get('/frontend/produk-list/{slug}', 'Frontend\ProdukController@produk_list');
Route::get('/frontend/produk-search', 'Frontend\ProdukController@produk_search');
Route::get('/frontend/produk-detail/{id}', 'Frontend\ProdukController@produk_detail');
Route::get('/frontend/produk-cart', 'Frontend\ProdukController@produk_cart')->name('produk_cart');

Route::post('/frontend/add_cart', 'Frontend\ProdukController@add_cart')->name('add_cart');
Route::get('/frontend/details', 'Frontend\ProdukController@details')->name('details_cart');
Route::get('/frontend/load_items', 'Frontend\ProdukController@load_items')->name('load_items');
Route::get('/frontend/delete_item/{id}', 'Frontend\ProdukController@delete_item')->name('delete_item');
Route::get('/frontend/clear_cart', 'Frontend\ProdukController@clear_cart')->name('clear_cart');
Route::get('/frontend/checkout', 'Frontend\CheckoutController@index')->name('checkout');


Route::post('/frontend/order_simpan', 'Frontend\CheckoutController@order_simpan')->name('order_simpan');
Route::get('/frontend/order_sukses/{order_kode}', 'Frontend\CheckoutController@order_sukses')->name('order_sukses');





// Route::group(['middleware' => ['web', 'auth', 'roles']], function() {
//     Route::group(['roles' => 'user'],function() {
//         Route::resource('jabatan', 'JabatanController');
//         Route::resource('karyawan', 'JabatanController');

//     });
//     Route::group(['roles' => 'admin',],function() {
//         Route::resource('divisi', 'DivisiController');

//     });

// });

